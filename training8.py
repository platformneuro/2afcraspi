import os, sys
import time
import billiard
import numpy as np
#import simpleaudio as sa
from videoGet4 import record
import yaml
import argparse
import shutil

################################################################################
# Training                                                                     #
# The animal has to report left for targets and rigth for non target words     #
# Can adjust the number of repeat of the correction trial                      #
# Can adjust the duration of punishment if incorrect response                  #
# Delay apply between central poke and response poke                           #
# Call :                                                                       #
#   python3 training7.py -a Gouda                                              #
################################################################################

# Parse inputs
parser = argparse.ArgumentParser(description='Main script for training ferrets on 2AFC.')
parser.add_argument('-a', dest='indiv', default='testFerret',type=str, help='Animal''s name.')
parser.add_argument('-ssh', dest='SSH', default=1,type=int, help='Are you using SSH?')
args = parser.parse_args()

data_path = '/home/lsp/Desktop/2afcraspi/Data' #path to where to put the data files of the experiment
stimPath = '/home/lsp/Desktop/2afcraspi/stimuli'
indiv = args.indiv

#print(args.SSH)
if args.SSH==0:
    import AFCutils as fct
else:
    import AFCutilsSSH as fct


# Read parameter file for that ferret
with open('parameters_' + indiv + '.yaml') as file:
    param = yaml.load(file, Loader=yaml.FullLoader)

tag = ['L','C','R']

# Load sounds
stimNames, waveObj = fct.bufferingSounds(param['targets'],stimPath)
LR_target = np.random.randint(2)

# Set pins
GPIO, led_pin, valve_pin, piezo_pin, detector_pin = fct.GPIOsetup()

# Billiard process creation
rewProcR = billiard.Process(target=fct.deliverRew,args=(valve_pin[2],GPIO,param['solOpenDurR']))
rewProcL = billiard.Process(target=fct.deliverRew,args=(valve_pin[0],GPIO,param['solOpenDurL']))
rewProcC = billiard.Process(target=fct.deliverRew,args=(valve_pin[1],GPIO,param['solOpenDurC']))

# keyboard listener
kb = fct.KeyboardListenerHandle().start()
# print(kb.inTest)

#Initialise lists for storage of reward and lick sides and times
pokeList = []
rewList = []
sndList = []
lickList = []

# Initialize flags
# Convention : [Left poke, Center poke, Rigth poke]
inTest = True
inPk = [False,False,False]
pked = [False,False,False]
# collected = [True,True]
cTrial = False
hasPokedCentral = False
hasPlayedSound = False
inPunishment = False
goSignal = True
lateral_rew_available = False

# Initialize counters
nCorrectionTrialsDone = 0
nRews = 0
trialCount = 0

#create a new data file for this session
data_filewriter,data_file,n_session,fileName = fct.CreatDataFile(data_path, indiv)
print(fileName)

# Write informations about the session
infoStr = ['script:' + sys.argv[0],
     'cTrial:' + str(param['correctionTrials']),
     'nCorrectionTrials:' + str(param['nCorrectionTrials']),
     'delay:' + str(param['timeInPoke']),
     'punishment:' + str(param['punishment']),
     'punishmentDur:' + str(param['punishmentDur']),
     'target:' + '-'.join(param['targets']),
     'soundPick:' + str(param['soundPickMethod'])]

print(infoStr)
fct.send_data(infoStr, data_filewriter, data_file)

# Start recording video
print('starting video feed...')
filepath = os.path.join('.', 'video', indiv, fileName)
video_getter = record(0, filepath, param['videoFPS']).start()

#Initialise relevant timers
pokeT = time.time()
sendT = time.time()
pokeTC = time.time()
lickT = time.time()
pkSt = [time.time(),time.time(),time.time()]
soundTime = time.time()
rewT = time.time()
startT = time.time() #Define start time
# prevL = time.time()

print('ready !')
try:
    while inTest:
        time.sleep(0.002)

        # Left poke
        if not GPIO.input(detector_pin[0]):
            if not inPk[0]:
                #print('here 1')
                pkSt[0] = time.time()
                inPk[0] = True

            else:
                if ((time.time() - pkSt[0])>param['rewardPortDelay'] and not pked[0]):
                    print('Poke Left')
                    pokeT = time.time()
                    pokeList.append([pokeT - startT,'L'])

                    pked[0] = True
                    if lateral_rew_available and (time.time()-rewT)>param['minIPI']:
                        if LR_target == 0: # good response
                            print('good response')
                            cTrial = False
                            rewT = time.time()
                            _ = fct.rew_action(0,rewProcR,rewProcL)
                            rewProcR = billiard.Process(target=fct.deliverRew,args=(valve_pin[2],GPIO,param['solOpenDurR']))
                            rewProcL = billiard.Process(target=fct.deliverRew,args=(valve_pin[0],GPIO,param['solOpenDurL']))

                            nRews += 1
                            rewList.append([rewT - startT,'L'])
                            lateral_rew_available = False
                            # collected[0] = True
                        elif LR_target == 1: # wrong response
                            print('wrong response')
                            if nCorrectionTrialsDone < param['nCorrectionTrials']:
                                cTrial = True
                            else:
                                cTrial = False
                            if param['punishment']:
                                inPunishment = True
                                punishmentStart = time.time()
                                print('Punished for ' + str(param['punishmentDur']) + 'sec !')
                            lateral_rew_available = False
                    else:
                        # Check if aborted trial
                        if hasPlayedSound and not lateral_rew_available and not goSignal:
                            print('Aborted trial')
                            hasPlayedSound = False
                            hasPokedCentral = False

        else:
            inPk[0] = False
            pked[0] = False

        # Rigth poke
        if not GPIO.input(detector_pin[2]):
            if not inPk[2]:
                pkSt[2] = time.time()
                inPk[2] = True

            else:
                if ((time.time() - pkSt[2])>param['rewardPortDelay'] and not pked[2]):
                    print('poke Right')
                    pokeT = time.time()
                    pokeList.append([pokeT -startT,'R'])

                    pked[2] = True
                    if lateral_rew_available and (time.time()-rewT)>param['minIPI']:
                        if LR_target==1: # good response
                            print('good response')
                            cTrial = False
                            rewT = time.time()
                            _ = fct.rew_action(1,rewProcR,rewProcL)
                            rewProcR = billiard.Process(target=fct.deliverRew,args=(valve_pin[2],GPIO,param['solOpenDurR']))
                            rewProcL = billiard.Process(target=fct.deliverRew,args=(valve_pin[0],GPIO,param['solOpenDurL']))

                            nRews += 1
                            rewList.append([time.time() - startT,'R'])
                            lateral_rew_available = False
                            # collected[1] = True
                        elif LR_target==0: # wrong response
                            print('wrong response')
                            if nCorrectionTrialsDone < param['nCorrectionTrials']:
                                cTrial = True
                            else:
                                cTrial = False
                            if param['punishment']:
                                inPunishment = True
                                punishmentStart = time.time()
                                print('Punished for ' + str(param['punishmentDur']) + 'sec !')
                            lateral_rew_available = False
                    else:
                        # Check if aborted trial
                        if hasPlayedSound and not lateral_rew_available and not goSignal:
                            print('Aborted trial')
                            hasPlayedSound = False
                            hasPokedCentral = False
        else:
            inPk[2]=False
            pked[2] = False

        #central poke
        if not GPIO.input(detector_pin[1]):
            #print("here")
            if not inPk[1]:
                pkSt[1] = time.time()
                inPk[1] = True

            else:
                if ((time.time() - pkSt[1])>param['centralPortDelay'] and not pked[1] and not hasPokedCentral):
                    print('poke Center')
                    pked[1] = True
                    hasPokedCentral = True
                    pokeT = time.time()
                    pokeList.append([pokeT - startT,'C'])
                    # prevL = time.time()
                    if not lateral_rew_available and not inPunishment:
                        if param['correctionTrials'] and cTrial:
                            nCorrectionTrialsDone = nCorrectionTrialsDone + 1
                            Ctstr = 'CoTr'
                        else:
                            LR_target, targetID, soundID = fct.pickSound(stimNames,param['soundPickMethod'])
                            nCorrectionTrialsDone = 0
                            Ctstr = ''
                        rewList.append([time.time() - startT,'C'])
                        print('playing ' + stimNames[targetID][LR_target][soundID] + ' - ' + Ctstr)
                        soundTime = time.time()
                        hasPlayedSound = True
                        trialCount += 1
                        playObj = waveObj[targetID][LR_target][soundID].play()
                        pokeTC = time.time()
                        sndList.append([pokeTC-startT,str(LR_target) + stimNames[targetID][LR_target][soundID] + Ctstr + 'Aborted'])
                        goSignal = False
        else:
            inPk[1] = False
            pked[1] = False

        # Wait before making reward available
        if (pkSt[0]-pkSt[1]) < 0 and (pkSt[2]-pkSt[1]) < 0 and (time.time() - soundTime)>param['timeInPoke'] and not inPunishment and not goSignal and hasPlayedSound:
            print('go get that reward !')
            sndList[-1][1] = sndList[-1][1].replace('Aborted','')
            goSignal = True
            lateral_rew_available = True
            hasPokedCentral = False

        # reset hasPokeCentral timeInPoke seconds after central poke
        if (hasPokedCentral and (time.time()-pkSt[1] > (param['timeInPoke'] + 2))):
            hasPokedCentral = False
            #if cTrial == True:
            #    cTrial = False
                
        # Reset punishment
        if param['punishment'] and inPunishment:
            if (time.time() - punishmentStart) > param['punishmentDur']:
                inPunishment = False
                print('Punishment is over.')

        # Count licks
        for i in range(3):
            if GPIO.input(piezo_pin[i]) == True and (time.time()-lickT)>param['minILI']:
                lickT = time.time()
                lickList.append([lickT - startT, tag[i]])
                print('lick ' + tag[i])

        # Send data every 5 second
        if (time.time()-sendT>5) and goSignal:
            pokeStr = 'pokeList:' + '-'.join([str(np.round(entry[0], decimals=3))+entry[1] for entry in pokeList])
            rewStr = 'rewList:' + '-'.join([str(np.round(entry[0], decimals=3))+entry[1] for entry in rewList])
            sndStr = 'sndList:' + '-'.join([str(np.round(entry[0], decimals=3))+entry[1] for entry in sndList])
            lickStr = 'lickList:' + '-'.join([str(np.round(entry[0], decimals=3))+entry[1] for entry in lickList])
            #sendStr = ','.join([rewStr,pokeStr,sndStr,lickStr])
            sendStr = [rewStr,pokeStr,sndStr,lickStr]
            #print(pokeStr)
            #print(sendStr)
            sendProc = billiard.Process(target=fct.send_data, args=(sendStr,data_filewriter,data_file))
            sendProc.start()
            print('seeeeeending')
            sendT = time.time()
            # Add ckeck if data has been sent before erasing the variables !
            pokeList = []; rewList = []; sndList = []; lickList = []

        # Check uncollected rewards
        if (time.time()-pokeTC > param['intervalDur']) and lateral_rew_available:
            lateral_rew_available = False
            # collected = [True,True,True]

        # Check total number of rewards
        if nRews > param['maxRews']:
            inTest = False
            print('Max reward number reached. Ending training.')
            
        # Check if idle for more than 10 minutes since last central poke (forget to turn task off)
        if time.time() - pokeT > 600:
            inTest = False
            print('No activity in the last 10 minutes. Ending training.')
        
        # assess keyboard status
        if not kb.inTest:
            inTest = False
except :
    print('Terminating task')

# Sending last batch of data
pokeStr = 'pokeList:' + '-'.join([str(np.round(entry[0],decimals=3))+entry[1] for entry in pokeList])
rewStr = 'rewList:' + '-'.join([str(np.round(entry[0],decimals=3))+entry[1] for entry in rewList])
sndStr = 'sndList:' + '-'.join([str(np.round(entry[0],decimals=3))+entry[1] for entry in sndList])
lickStr = 'lickList:' + '-'.join([str(np.round(entry[0],decimals=3))+entry[1] for entry in lickList])
sendStr = [rewStr,pokeStr,sndStr,lickStr]

sendProc = billiard.Process(target=fct.send_data,args=(sendStr,data_filewriter,data_file))
sendProc.start()

print(fileName)
print(infoStr)
print('Trial done : ' + str(trialCount))
print('Elapsed time : ' + str(round((time.time()-startT)/60)) + ' minutes')

# Cleaning GPIO pins
GPIO.cleanup()

# Copy session file in Data raw
print('Copying raw data...')
shutil.copy2(os.path.join(data_path,indiv,fileName + '.csv'), os.path.join(data_path.replace('Data','DataRaw'),indiv,fileName + '.csv'))

# Stop video recording
video_getter.stop()
#print(video_getter.IOtime)

time.sleep(2)

print('All done')
