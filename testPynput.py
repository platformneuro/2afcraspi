from pynput.keyboard import Key, Listener
import billiard

def on_press(key):
    print('{0} pressed'.format(key))
    
def on_release(key):
    print('{0} released'.format(key))
    zed = True
    if key == Key.esc:
        print('terminate')
        exit(0)
        zed = False
        # stop listener
        return zed

# collect events until released
def listen():
    with Listener(
            on_press=on_press,
            on_release=on_release) as listener:
        listener.join()
        zed = False
        return zed
 
rewProcR = billiard.Process(target=listen,args=())
rewProcR.start()
zed = True

while zed:
    on_release()
    print('here')   
    
