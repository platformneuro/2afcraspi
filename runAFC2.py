import datetime
from time import time
import natsort
import csv
import re
import os
import sys
import getopt
import numpy.random as rnd
#import RPi.GPIO as GPIO
import time
from pynput import keyboard

class runTask:
    def __init__(self, indiv = ""):
        self.isRunning = False

    def readcentral(self):
        while self.isRunning:
            if not GPIO.input(detector_pin[1]):
            #print("here")
                if not inPk[1]:
                    pkSt[1] = time.time()
                    inPk[1] = True

                else:
                    if ((time.time() - pkSt[1])>centralPortDelay and not pked[1] and not hasPokedCentral):
                        print('poke Center')
                        pked[1] = True
                        hasPokedCentral = True
                        pokeT = time.time()
                        pokeList.append([pokeT - start,'C'])
                        prevL = time.time()
                        if not lateral_rew_available:
                            if correctionTrials and cTrial:
                                nCorrectionTrialsDone = nCorrectionTrialsDone + 1
                                Ctstr = 'CoTr'
                            else:
                                LR_target, targetID, soundID = pickSound(stimNames,soundPickMethod)
                                nCorrectionTrialsDone = 0
                                Ctstr = ''
                            rewList.append([time.time() - start,'C'])
                            print('playing ' + stimNames[targetID][LR_target][soundID] + ' - ' + Ctstr)
                            waveObj[targetID][LR_target][soundID].play()
                            pokeTC = time.time()
                            sndList.append([pokeTC-start,str(LR_target)+stimNames[targetID][LR_target][soundID] + Ctstr])
                    
                    elif (pked[1] and (time.time() - pkSt[1])>timeInPoke): # keep in central poke, end of waiting time
                        print('go get that reward !')
                        lateral_rew_available = True
                        hasPokedCentral = False           
            else:
                inPk[1]=False
                pked[1] = False

    def start(self)
        self.isRunning = True