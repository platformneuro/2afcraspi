from threading import Thread
import cv2
from time import time, sleep
import csv
import argparse
import queue

#ffmpeg -i Oscypek_session10_281122.avi -vcodec h264 Oscypek_session10_281122.mp4

class record:

    def __init__(self, src=0,filepath='./outpy',fps=0,record=1,display=0):
        #self.stream = cv2.VideoCapture(src,cv2.CAP_DSHOW)
        self.stream = cv2.VideoCapture(src)
        self.frame_width = int(self.stream.get(3))
        self.frame_height = int(self.stream.get(4))
        #print(self.frame_width)
        #print(self.frame_height)
        self.grabbed = self.stream.read()[0]
        cameraFps = self.stream.get(cv2.CAP_PROP_FPS)
        #self.stream.set(cv2.CAP_PROP_BUFFERSIZE,2)
        self.IOtime = list()
        self.record = record
        self.display = display
        self.Nframes = 0
        self.filepath = filepath
        # Check video fps
        if cameraFps == 0:
            print("warning : could not read camera fps.")
            if fps == 0:
                raise ValueError('Set fps is too low.')
        else:
            if fps == 0:
                fps = cameraFps
            if fps > cameraFps:
                raise ValueError('set fps is superior to camera fps.')

        self.frameDuration = 1/fps
        if self.record==1:
            self.out = cv2.VideoWriter(filepath + '.avi',cv2.VideoWriter_fourcc('M','J','P','G'),fps,(self.frame_width,self.frame_height))
            #self.out = cv2.VideoWriter(filepath + '.mp4',cv2.VideoWriter_fourcc('H','2','6','4'),fps,(self.frame_width,self.frame_height),0)
            self.q = queue.Queue()
            Thread(target=self.writeFrame, daemon=True).start()
        #self.out = cv2.VideoWriter(filepath + '.avi',cv2.VideoWriter_fourcc('a','v','c','1'),fps, (self.frame_width,self.frame_height))
        
    def start(self):
        print("Video recording started.")
        self.stopped = False
        self.startTime = time()
        self.currentTime = time()
        self.IOtime = list()
        Thread(target=self.get, args=()).start()
        #billiard.Process(target=self.get, args=()).start()
        return self

    def get(self):
        while not self.stopped:
            sleep(0.002)
            
            if not self.grabbed:
                print("Grab failed.")
                self.stop()
            else:
                if (time() - self.currentTime) > self.frameDuration:
                    self.currentTime = time()
                    #self.IOtime.append(time()-self.startTime)
                    self.IOtime = (time()-self.startTime)
                    (self.grabbed, frame) = self.stream.read()
                    #frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

                    if self.record==1:
                        #self.out.write(frame)
                        with open(self.filepath+'_frames.csv', 'a', newline='') as f:
                            writer = csv.writer(f)
                            writer.writerow((self.IOtime,))
                        self.q.put(frame)

                    if self.display==1:
                        cv2.imshow('video stream',frame)
                        cv2.waitKey(1)

                    self.Nframes += 1
        else:
            self.endStream()

    def writeFrame(self):
        while True:
            frame = self.q.get()
            self.out.write(frame)

    def stop(self):
        self.stopped = True
        self.endTime = time()
    
    def endStream(self):
        #self.q.join() # wait for last frames to be written
        #sleep(10)
        #(self.grabbed, self.frame) = self.stream.read()
        self.stream.release()
        if self.record==1:
            print('Emptying video queue...')
            while not self.q.empty():
                sleep(0.5)
            self.out.release()
        cv2.destroyAllWindows()
        print("Video recording ended.")
 
def main():
    parser = argparse.ArgumentParser(description='Record and/or display videos using cv2.')
    parser.add_argument('-f', dest='filepath', default='./outpy',type=str, help='output file name')
    parser.add_argument('-src', dest='src', default=0, type=int, help='Video source')
    parser.add_argument('-fps', dest='fps', default=0, type=int, help='Video sampling rate')
    parser.add_argument('-r', dest='record', default=1, type=int, help='Recording switch')
    parser.add_argument('-d', dest='display', default=0, type=int, help='Display switch')
    args = parser.parse_args()
    cam = record(src=args.src,filepath=args.filepath,fps=args.fps,record=args.record,display=args.display)
    cam.start()
    input('Press enter to terminate recording: ')
    cam.stop()

if __name__ == "__main__":
    main()
