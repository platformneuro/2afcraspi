from threading import Thread, Lock
import cv2
from time import time, sleep
import csv
import argparse
import queue
import RPi.GPIO as GPIO

class record:

    def __init__(self, src=0,filepath='./outpy',fps=0,record=1,display=0):
        # initialization of video input
        #self.stream = cv2.VideoCapture(src,cv2.CAP_DSHOW)
        self.stream = cv2.VideoCapture(src)
        self.frame_width = int(self.stream.get(3))
        self.frame_height = int(self.stream.get(4))
        #print(self.frame_width)
        #print(self.frame_height)
        self.grabbed = self.stream.read()[0]
        cameraFps = self.stream.get(cv2.CAP_PROP_FPS)
        #self.stream.set(cv2.CAP_PROP_BUFFERSIZE,2)
        self.IOtime = list()
        self.record = record
        self.display = display
        self.Nframes = 0
        self.filepath = filepath
        # Check video fps
        if cameraFps == 0:
            print("warning : could not read camera fps.")
            if fps == 0:
                raise ValueError('Set fps is too low.')
        else:
            if fps == 0:
                fps = cameraFps
            if fps > cameraFps:
                raise ValueError('set fps is superior to camera fps.')
        
        #Iitialization of video recording
        self.frameDuration = 1/fps
        if self.record==1:
            # self.out = cv2.VideoWriter(filepath + '.avi',cv2.VideoWriter_fourcc('a','v','c','1'),fps, (self.frame_width,self.frame_height))
            # self.out = cv2.VideoWriter(filepath + '.avi',cv2.VideoWriter_fourcc('M','J','P','G'),fps,(self.frame_width,self.frame_height),0)
            self.out = cv2.VideoWriter(filepath + '.avi',cv2.VideoWriter_fourcc('H','2','6','4'),fps,(self.frame_width,self.frame_height),0)
            self.q = queue.Queue()
            self.error_q = queue.Queue()
            Thread(target=self.writeFrame, daemon=True).start()
            Thread(target=self.read_error_q, daemon=True).start()
        
        # GPIO initialization for sending frame pulse
        GPIO.setmode(GPIO.BCM)
        self.pulsePin = 7
        self.pulsePin_error = 8
        GPIO.setup(self.pulsePin,GPIO.OUT,initial = GPIO.LOW)
        GPIO.setup(self.pulsePin_error,GPIO.OUT,initial = GPIO.LOW)
        self.GPIO=GPIO
        
    def start(self):
        print("Video recording started.")
        self.stopped = False
        self.startTime = time()
        self.currentTime = time()
        self.IOtime = list()
        Thread(target=self.get, args=()).start()
        #billiard.Process(target=self.get, args=()).start()
        return self

    def get(self):
        while not self.stopped:
            sleep(0.002)
            
            if not self.grabbed:
                print("Grab failed.")
                self.stop()
            else:
                if (time() - self.currentTime) > self.frameDuration:
                    self.currentTime = time()
                    #self.IOtime.append(time()-self.startTime)
                    self.IOtime = (time()-self.startTime)
                    # Send frame pulse to the raspi
                     # self.send_pulse()
                    Thread(target=self.send_pulse, args=()).start()
                    (self.grabbed, frame) = self.stream.read()
                    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

                    if self.record==1:
                        #self.out.write(frame)
                        with Lock():
                            try:
                                self.q.put([frame, self.IOtime],block=True,timeout=self.frameDuration * 0.9) # Should return a full exception if not able to put frame in queue during 90% of time of a frame
                            except Exception as e:
                                self.error_q.put(e)
                                Thread(target=self.send_pulse_error, args=()).start()
                        # Alternatively this code should have the same behavior :
                        # while self.q.full():
                        #     if ((time() - self.currentTime) > self.frameDuration * 0.9):
                        #         print('Full queue, skipped frame.')
                        #         break
                        # else:
                        #     self.q.put(frame)

                    if self.display==1:
                        cv2.imshow('video stream',frame)
                        cv2.waitKey(1)

                    self.Nframes += 1
        else:
            self.endStream()

    def send_pulse(self):
        self.GPIO.output(self.pulsePin,True)
        sleep(0.002)
        self.GPIO.output(self.pulsePin,False)
    
    def send_pulse_error(self):
        self.GPIO.output(self.pulsePin_error,True)
        sleep(0.002)
        self.GPIO.output(self.pulsePin_error,False)

    def writeFrame(self):
        while True:
            try:
                with Lock():
                    frame = self.q.get(block=True, timeout=None) # block execution until there is something to get in the queue
                self.out.write(frame[0])
                with open(self.filepath+'_frames.csv', 'a', newline='') as f:
                                    writer = csv.writer(f)
                                    writer.writerow((frame[1],))
            except Exception as e:
                self.error_q.put(e)

    def read_error_q(self):
        while True:
            sleep(0.002)
            if not self.error_q.empty():
            # get the exception object from the queue and re-raise it in the main thread
                e = self.error_q.get()
                print('Rethrowing thread error:')
                print(e)

    def stop(self):
        self.stopped = True
        self.endTime = time()
    
    def endStream(self):
        #self.q.join() # wait for last frames to be written
        #sleep(10)
        #(self.grabbed, self.frame) = self.stream.read()
        self.stream.release()
        if self.record==1:
            print('Emptying video queue...')
            while not self.q.empty():
                sleep(1)
                print('...')
            self.out.release()
        cv2.destroyAllWindows()
        
        self.GPIO.cleanup()
        
        print("Video recording ended.")

def main():
    parser = argparse.ArgumentParser(description='Record and/or display videos using cv2.')
    parser.add_argument('-f', dest='filepath', default='./outpy',type=str, help='output file name')
    parser.add_argument('-src', dest='src', default=0, type=int, help='Video source')
    parser.add_argument('-fps', dest='fps', default=0, type=int, help='Video sampling rate')
    parser.add_argument('-r', dest='record', default=1, type=int, help='Recording switch')
    parser.add_argument('-d', dest='display', default=0, type=int, help='Display switch')
    args = parser.parse_args()
    cam = record(src=args.src,filepath=args.filepath,fps=args.fps,record=args.record,display=args.display)
    cam.start()
    input('Press enter to terminate recording: ')
    cam.stop()

if __name__ == "__main__":
    main()

