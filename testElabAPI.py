import elabapy
import json
from requests.exceptions import HTTPError

# initialize the manager with an endpoint and your token
manager = elabapy.Manager(endpoint="https://129.199.80.246/", token="888993eb4e5caf2b36b9486a7ea0b6aa018edb7d0e8ea413484d5a1fc7d227990e5f68a8cf7d6bc4d980")
# get experiment with id 42
try:
    exp = manager.get_experiment(705)
    print(json.dumps(exp, indent=4, sort_keys=True))
# if something goes wrong, the corresponding HTTPError will be raised
except HTTPError as e:
    print(e)