import datetime
from time import time
import natsort
import csv
import re
import os
import sys
import getopt
import numpy.random as rnd
#import RPi.GPIO as GPIO
import time
from pynput import keyboard
from AFCutils import GPIOsetup
from threading import Thread


class pinReader:

    def __init__(self, indiv = ""):
        # setup parameters
        if len(indiv)==0:
            indiv = input("Enter ferret name : ")
        self.GPIO, self.led_pin, self.valve_pin, self.piezo_pin, self.detector_pin = GPIOsetup()
        self.readout = {'inPortLeft': False,
        }


    def start(self):
        self.startTime = time()
        self.currentTime = time()
        self.IOtime = list()    
        Thread(target=self.get, args=()).start()
        #billiard.Process(target=self.get, args=()).start()
        return self

    def get(self):
        while not self.stopped:
            # Left poke
            if not self.GPIO.input(self.detector_pin[0]):
                if not inPk[0]:
                    #print('here 1')
                    pkSt[0] = time.time()
                    inPk[0] = True

                else:
                    if ((time.time() - pkSt[0])>rewardPortDelay and not pked[0]):
                        print('Poke Left')
                        pokeT = time.time()
                        pokeList.append([pokeT -start,'L'])

                        pked[0] = True

            else:
                inPk[0]=False
                pked[0] = False


def main(argv):
    # Parse inputs
    indiv = ''
    loopName = ''
    try:
        opts, args = getopt.getopt(argv,"hf:s:",["indiv=","loopName="])
    except getopt.GetoptError:
        print('runAFC.py -f <ferret name> -s <script name>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('runAFC.py -f <ferretName> -s <scriptName>')
            sys.exit()
        elif opt in ("-f", "--ferretName"):
            indiv = arg
        elif opt in ("-s", "--scriptName"):
            loopName = arg
    if len(loopName)==0:
        loopName = input("Enter loop name : ")
    print('Ferret : ' + indiv)
    print('script : ' + loopName)

    # call class
    f = afcFunctions(indiv)
    print(f.p.led_pin)
    
    l = __import__(loopName)
    l.run()
    #print(l)


if __name__ == "__main__":
    # execute only if run as a script
    main(sys.argv[1:])

