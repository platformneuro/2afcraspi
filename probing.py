import os, sys
import time
import billiard
import numpy as np
#import simpleaudio as sa
from videoGet4 import record
import shutil
import AFCutils as fct
import traceback

################################################################################
# Probing                                                                      #
# Based on training7.py                                                        #
# The animal has to report left for targets and rigth for non target words     #
# Can adjust the number of repeat of the correction trial                      #
# Can adjust the duration of punishment if incorrect response                  #
################################################################################

# Parse inputs
parser = fct.setParser()
args = parser.parse_args()

# Read parameter file for that ferret
param = fct.read_param_file(args.indiv)

tag = ['L','C','R']

# Load sounds
stimNames, waveObj = fct.bufferingSounds_new2(param['targets'],fct.stimPath,param['loudnessDifference'])

# Set pins
GPIO, led_pin, valve_pin, piezo_pin, detector_pin = fct.GPIOsetup()

# Billiard process creation
rewProcR = billiard.Process(target=fct.deliverRew,args=(valve_pin[2],GPIO,param['solOpenDurR']))
rewProcL = billiard.Process(target=fct.deliverRew,args=(valve_pin[0],GPIO,param['solOpenDurL']))
rewProcC = billiard.Process(target=fct.deliverRew,args=(valve_pin[1],GPIO,param['solOpenDurC']))
#échange valves 1 et 2 car valve de droite ne marche plus
# keyboard listener
kb = fct.KeyboardListenerHandle().start()
inTest = True
nRews = 0

#Initialise lists for storage of reward and lick sides and times
pokeList = []
rewList = []
sndList = []
lickList = []

#Initialise relevant timers
pokeT = time.time()
prevL = time.time()
sendT = time.time()
pokeTC = time.time()
lickT = time.time()
rewT = time.time()

LR_target = np.random.randint(2)
lateral_rew_available = False

# Left, Center, Rigth
inPk = [False,False,False]
pkSt = [time.time(),time.time(),time.time()]
pked = [False,False,False]
collected = [True,True]
cTrial = False
hasPokedCentral = False
inPunishment = False
goSignal = True

#create a new data file for this session
data_filewriter,data_file,n_session,fileName = fct.CreatDataFile(fct.data_path, args.indiv, args.noNAS)
print(fileName)

# Write informations about the session
# infoStr = ['script:' + sys.argv[0],
#      'cTrial:' + str(param['correctionTrials']),
#      'nCorrectionTrials:' + str(param['nCorrectionTrials']),
#      'delay:' + str(param['timeInPoke']),
#      'punishment:' + str(param['punishment']),
#      'punishmentDur:' + str(param['punishmentDur']),
#      'target left:' + '-'.join(param['targets']['left']),
#      'target right:' + '-'.join(param['targets']['right']),
#      'target probe:' + '-'.join(param['targets']['probe']),
#      'soundPick:' + str(param['soundPickMethod'])]
# print(infoStr)
# Write info to datafile
# fct.send_data(infoStr, data_filewriter, data_file)

fct.writeInfoStr(param,data_filewriter,data_file)

# Start recording video
if param['recordVideo']:
    print('starting video feed...')
    filepath = os.path.join('.', 'video', args.indiv, fileName)
    video_getter = record(0, filepath, param['videoFPS']).start()

#Define start time
start = time.time()
nCorrectionTrialsDone = 0
trialCount = 0

print('ready !')
try:
    while inTest:
        time.sleep(0.002)

        # Left poke
        if not GPIO.input(detector_pin[0]):
            if not inPk[0]:
                #print('here 1')
                pkSt[0] = time.time()
                inPk[0] = True

            else:
                if ((time.time() - pkSt[0])>param['rewardPortDelay'] and not pked[0]):
                    print('poke left')
                    pokeT = time.time()
                    pokeList.append([pokeT -start,'L'])

                    pked[0] = True
                    if lateral_rew_available and (time.time()-rewT)>param['minIPI']:
                        if LR_target == 0: # good response
                            print('good response')
                            cTrial = False
                            rewT = time.time()
                            _ = fct.rew_action(0,rewProcR,rewProcL)
                            rewProcR = billiard.Process(target=fct.deliverRew,args=(valve_pin[2],GPIO,param['solOpenDurR']))
                            rewProcL = billiard.Process(target=fct.deliverRew,args=(valve_pin[0],GPIO,param['solOpenDurL']))

                            nRews += 1
                            rewList.append([rewT - start,'L'])
                            lateral_rew_available = False
                            # collected[0] = True
                        elif LR_target == 1: # wrong response
                            print('wrong response')
                            if nCorrectionTrialsDone < param['nCorrectionTrials']:
                                cTrial = True
                            else:
                                cTrial = False
                            if param['punishment']:
                                inPunishment = True
                                punishmentStart = time.time()
                                print('punished for ' + str(param['punishmentDur']) + 'sec !')
                            lateral_rew_available = False
                        elif LR_target == 2: #probe trial
                            print('probe trial - no reward')
                            cTrial = False
                            lateral_rew_available = False

        else:
            inPk[0]=False
            pked[0] = False

        # Rigth poke
        if not GPIO.input(detector_pin[2]):
            if not inPk[2]:
                pkSt[2] = time.time()
                inPk[2] = True

            else:
                if ((time.time() - pkSt[2])>param['rewardPortDelay'] and not pked[2]):
                    print('poke right')
                    pokeT = time.time()
                    pokeList.append([pokeT -start,'R'])

                    pked[2] = True
                    if lateral_rew_available and (time.time()-rewT)>param['minIPI']:
                        if LR_target==1: # good response
                            print('good response')
                            cTrial = False
                            rewT = time.time()
                            _ = fct.rew_action(1,rewProcR,rewProcL)
                            rewProcR = billiard.Process(target=fct.deliverRew,args=(valve_pin[2],GPIO,param['solOpenDurR']))
                            rewProcL = billiard.Process(target=fct.deliverRew,args=(valve_pin[0],GPIO,param['solOpenDurL']))

                            nRews += 1
                            rewList.append([time.time() - start,'R'])
                            lateral_rew_available = False
                            # collected[1] = True
                        elif LR_target==0: # wrong response
                            print('wrong response')
                            if nCorrectionTrialsDone < param['nCorrectionTrials']:
                                cTrial = True
                            else:
                                cTrial = False
                            if param['punishment']:
                                inPunishment = True
                                punishmentStart = time.time()
                                print('punished for ' + str(param['punishmentDur']) + 'sec !')
                            lateral_rew_available = False 
                        elif LR_target == 2: #probe trial
                            print('probe trial - no reward')
                            cTrial = False
                            lateral_rew_available = False

        else:
            inPk[2]=False
            pked[2] = False

        #central poke
        if not GPIO.input(detector_pin[1]):
            #print("here")
            if not inPk[1]:
                pkSt[1] = time.time()
                inPk[1] = True

            else:
                if ((time.time() - pkSt[1])>param['centralPortDelay'] and not pked[1] and not hasPokedCentral):
                    print('poke center')
                    pked[1] = True
                    hasPokedCentral = True
                    pokeT = time.time()
                    pokeList.append([pokeT - start,'C'])
                    prevL = time.time()
                    if not lateral_rew_available and not inPunishment:
                        if param['correctionTrials'] and cTrial:
                            nCorrectionTrialsDone = nCorrectionTrialsDone + 1
                            Ctstr = 'CoTr'
                        else:
                            LR_target, targetID, soundID = fct.pickSound_probe(stimNames,param['soundPickMethod'],param['trialProbabilities'])
                            nCorrectionTrialsDone = 0
                            Ctstr = ''
                        rewList.append([time.time() - start,'C'])
                        print('playing ' + stimNames[targetID][LR_target][soundID] + ' - ' + Ctstr)
                        trialCount += 1
                        playObj = waveObj[targetID][LR_target][soundID].play()
                        pokeTC = time.time()
                        sndList.append([pokeTC-start,str(LR_target) + stimNames[targetID][LR_target][soundID] + Ctstr + 'Aborted'])
                        goSignal = False
                
                elif (pked[1] and (time.time() - pkSt[1])>param['timeInPoke']) and not inPunishment and not goSignal: # keep in central poke, end of waiting time
                #elif (pked[1] and not playObj.is_playing()) and not inPunishment and not goSignal: # keep in central poke, end of waiting time
                    print('go get that reward !')
                    sndList[-1][1] = sndList[-1][1].replace('Aborted','')
                    goSignal = True
                    lateral_rew_available = True
                    hasPokedCentral = False
                        
        else:
            inPk[1]=False
            pked[1] = False

        # Detect aborted trials
        

        # reset hasPokeCentral timeInPoke seconds after central poke
        if (hasPokedCentral and (time.time()-pkSt[1] > (param['timeInPoke'] + 2))):
            hasPokedCentral = False
            #if cTrial == True:
            #    cTrial = False
                
        # Reset punishment
        if param['punishment'] and inPunishment:
            if (time.time() - punishmentStart) > param['punishmentDur']:
                inPunishment = False
                print('punishment is over.')

        # Count licks
        #for i in range(3):
        #    if GPIO.input(piezo_pin[i]) == True and (time.time()-lickT)>param['minILI']:
        #        lickT = time.time()
        #        lickList.append([lickT - start,tag[i]])
        #       print('lick ' + tag[i])

        # Send data every 5 second 
        if (time.time()-sendT>5) and goSignal:
        
            pokeStr = 'pokeList:' + '-'.join([str(np.round(entry[0],decimals=3))+entry[1] for entry in pokeList])
            rewStr = 'rewList:' + '-'.join([str(np.round(entry[0],decimals=3))+entry[1] for entry in rewList])
            sndStr = 'sndList:' + '-'.join([str(np.round(entry[0],decimals=3))+entry[1] for entry in sndList])
            lickStr = 'lickList:' + '-'.join([str(np.round(entry[0],decimals=3))+entry[1] for entry in lickList])
            #sendStr = ','.join([rewStr,pokeStr,sndStr,lickStr])
            sendStr = [rewStr,pokeStr,sndStr,lickStr]
            #print(pokeStr)
            #print(sendStr)
            
            sendProc = billiard.Process(target=fct.send_data,args=(sendStr,data_filewriter,data_file))
            sendProc.start()
            print('seeeeeending')
            sendT = time.time()
            pokeList = []; rewList = []; sndList = []; lickList = []

        # Check uncollected rewards
        if (time.time()-pokeTC > param['intervalDur']) and lateral_rew_available:
            lateral_rew_available = False
            collected = [True,True,True]

        # Check total number of rewards
        if nRews > param['maxRews']:
            inTest = False
            print('Max reward number reached. Ending training.')
            
        # Check if idle for more than 10 minutes (forget to turn task off)
        if time.time() - pokeT > 600:
            inTest = False
            print('No activity in the last 10 minutes. Ending training.')
        
        # assess keyboard status
        if not kb.inTest:
            inTest = False
except :
    traceback.print_exc()
    pass

# Sending last batch of data
pokeStr = 'pokeList:' + '-'.join([str(np.round(entry[0],decimals=3))+entry[1] for entry in pokeList])
rewStr = 'rewList:' + '-'.join([str(np.round(entry[0],decimals=3))+entry[1] for entry in rewList])
sndStr = 'sndList:' + '-'.join([str(np.round(entry[0],decimals=3))+entry[1] for entry in sndList])
lickStr = 'lickList:' + '-'.join([str(np.round(entry[0],decimals=3))+entry[1] for entry in lickList])
sendStr = [rewStr,pokeStr,sndStr,lickStr]

sendProc = billiard.Process(target=fct.send_data,args=(sendStr,data_filewriter,data_file))
sendProc.start()

print('Time elapsed : ' + str(round((time.time() - start) / 60,2)) + ' minutes.')
print('Trial done : ' + str(trialCount))

# Cleaning GPIO pins
#GPIO.cleanup()

# Copy session file in Data raw
if ~args.noNAS:
    print('Copying raw data...')
    shutil.copyfile(os.path.join(fct.data_path,args.indiv,fileName + '.csv'), os.path.join(fct.data_path.replace('2afcraspi/DataRaw','dataPi'),'Data', args.indiv, fileName + '.csv'))  

# Stop video recording
if param['recordVideo']:
    video_getter.stop()
#print(video_getter.IOtime)

time.sleep(2)
print('All done')
