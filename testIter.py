import numpy.random as rnd
from numpy import sum

class targetOrder:
    def __init__(self,target, stimNames, soundPickMethod,NstimsPerCat):
        self.trialNumber = 1
        self.stimNames = stimNames
        self.NstimsPerCat = NstimsPerCat
        self.NtrialsMax = sum(NstimsPerCat)
        self.target = target
        self.soundPickMethod = soundPickMethod
        self.makeTrialVector()
        self.shuffleTrials()

    def __iter__(self):
        # Makes object iterable
        return self

    def __next__(self):
        # To to on each iter
        if self.trialNumber == self.NtrialsMax:
            self.trialNumber = 1
            # self.trialVector = self.makeTrialVector()
        self.trialNumber = self.trialNumber + 1
        # return self

    def makeTrialVector(self):
        # self.trialVector = list(range(self.NtrialsMax))
        for i,targetList in enumerate(self.soundPickMethod[self.target]): # target word
            # targetName = stimNames[targetID][LR_target][0]
            # targetName = targetName.split('_')[0] # only get the target name, not variant number
            # targetList = soundPickMethod[targetName]
            if targetList == 'all': # pick any available target
                # soundID = rnd.randint(len(self.stimNames[0][LR_target]))
                soundID = list(range(len(self.stimNames[0][i])))
            else: # pick from the indices provided in params
                # N = len(targetList)
                # soundID = targetList[rnd.randint(N)]
                soundID = self.soundPickMethod[self.target][i]
            if len(soundID) > self.NstimsPerCat:
                pass

        # else: # Non target word
        #     soundID = np.random.randint(len(stimNames[targetID][LR_target])) # pick any non target word
        # return LR_target, targetID, soundID
        return self

    def shuffleTrials(self):
        rnd.shuffle(self.trialVector)
        # items = [[1, 'A'], [2, 'A'], [6, 'B'], [3, 'B'], [4, 'C'], [5, 'C'], [7, 'F']]

        # import random

        # r = {b: random.random() for a, b in items}
        # items.sort(key=lambda item: r[item[1]])

        # print(items)
        return self

    def getSoundIndices(self):
        #return index in sound buffer structure
        idx = self.trialVector[self.trialNumber-1]
        return idx
