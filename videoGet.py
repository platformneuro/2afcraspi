from threading import Thread
#import billiard
import cv2
#import os
from time import time

class record:
    """
    Class that continuously gets frames from a VideoCapture object
    with a dedicated thread.
    """

    def __init__(self, src=0,filepath='./outpy',fps=0):
        #self.stream = cv2.VideoCapture(src,cv2.CAP_DSHOW)
        self.stream = cv2.VideoCapture(src)
        self.frame_width = int(self.stream.get(3))
        self.frame_height = int(self.stream.get(4))
        (self.grabbed, self.frame) = self.stream.read()
        cameraFps = self.stream.get(cv2.CAP_PROP_FPS)
        #self.stream.set(cv2.CAP_PROP_BUFFERSIZE,2)
        self.IOtime = list()
        self.display = True
        
        # Check video fps
        if cameraFps == 0:
            print("warning : could not read camera fps.")
            if fps == 0:
                raise ValueError('Set fps is too low.')
        else:
            if fps == 0:
                fps = cameraFps
            if fps > cameraFps:
                raise ValueError('set fps is superior to camera fps.')

        self.frameDuration = 1/fps
        self.stopped = False
        self.out = cv2.VideoWriter(filepath + '.avi',cv2.VideoWriter_fourcc('M','J','P','G'),fps, (self.frame_width,self.frame_height))
        #self.out = cv2.VideoWriter(filepath + '.avi',cv2.VideoWriter_fourcc('a','v','c','1'),fps, (self.frame_width,self.frame_height))
        
    def start(self):
        self.startTime = time()
        self.currentTime = time()
        self.IOtime = list()    
        Thread(target=self.get, args=()).start()
        #billiard.Process(target=self.get, args=()).start()
        return self

    def get(self):
        while not self.stopped:
            
            if not self.grabbed:
                print("Grab failed.")
                self.stop()
            else:
                if (time() - self.currentTime) > self.frameDuration:
                    self.currentTime = time()
                    #self.IOtime.append(time()-self.startTime)
                    (self.grabbed, self.frame) = self.stream.read()
                    self.out.write(self.frame)
                    if self.display:
                        cv2.imshow('image',self.frame)

        else:
            self.endStream()

    def stop(self):
        self.stopped = True
        self.endTime = time()
    
    def endStream(self):
        print("Video recording ended.")
        (self.grabbed, self.frame) = self.stream.read()
        self.stream.release()
        self.out.release()
        cv2.destroyAllWindows()
 

