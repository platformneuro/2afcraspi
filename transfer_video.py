import os
import subprocess
import argparse
import shutil

# Convert and transfer videos from raspi to NAS.

# Parse inputs
parser = argparse.ArgumentParser(description='Convert videos to mp4 h264 format and transfer them to the dataPi folder (mounted on NAS)')
parser.add_argument('-a','--list',nargs='+', dest='animalName', default='testFerret', help='Animal''s name(s).')
parser.add_argument('-p', dest='dataPath', default="/home/lsp/Desktop/2afcraspi/video", type=str, help='Path to video data files root folder.')
parser.add_argument('-pApp', dest='dataPathApp', default="/home/lsp/Desktop/dataPi/Video", type=str, help='Path to 2afcapp video folder.')
parser.add_argument('-rm', dest='remove', action='store_true', help='Remove source video after conversion and copy')
args = parser.parse_args()

# Function definition
def stripVidNames(pathList):
    strPath = [os.path.splitext(f)[0] for f in pathList if '_frames' not in f]
    return strPath
def checkMount(path):
    path = path + os.sep
    while True:
        if path == os.path.dirname(path):
            return False
        elif os.path.ismount(path):
            return True
        path = os.path.dirname(path)

# Check path validity
if not os.path.isdir(args.dataPath) or not os.path.isdir(args.dataPathApp):
    raise TypeError('Invalid directory')

# Check if dataPi is mounted
if not checkMount(args.dataPathApp):
    print('Warning : remote data folder is not mounted. Run ./mount_datapi.sh before transfering.')
    r = input('Interrupt transfer (Y/N) ? ')
    if r == 'Y' or r == 'y':
        raise KeyboardInterrupt('User interruption')

for animal in args.animalName:
    print('\n\n--- Animal : {} ---\n\n'.format(animal))
    path_videos = os.path.join(args.dataPath, animal)
    path_videos_remote = os.path.join(args.dataPathApp, animal)

    # Convert video
    # get video list
    video_list = stripVidNames(os.listdir(path_videos))
    # compare to remote video list
    video_list_remote = stripVidNames(set(os.listdir(path_videos_remote)))
    video_list_unique = [x + '.avi' for x in video_list if x not in video_list_remote]
    print('Videos to process : ' + str(video_list_unique))

    # compare to remote session folder, look for "_ignore" tags
    #-> Maybe not, it's more conservative to keep everything

    # convert videos that are only present locally
    for i,vid in enumerate(video_list_unique):
        print('\n--- Converting video {}/{} ---\n'.format(i+1,len(video_list_unique)))
        cmd = 'ffmpeg -i "{}" -vcodec h264 "{}"'.format(os.path.join(path_videos,vid), os.path.join(path_videos_remote,vid).replace(".avi",".mp4"))
        out = subprocess.run(cmd,shell=True)
        print('--- Convertion done ---')

        # move frames csv files to remote folder
        print('--- Copying video files ---')
        #shutil.copy(os.path.join(path_videos,vid).replace('.avi','_frames.csv'),path_videos_remote)
        cmd = 'cp {} {}'.format(os.path.join(path_videos,vid).replace('.avi','_frames.csv'), path_videos_remote)
        print('--- Copying done ---')

        # remove videos from local folder
        # Don't forget the frames csv files
        if args.remove:
            print("--- Removing files ---")
            os.remove(os.path.join(path_videos,vid))
            os.remove(os.path.join(path_videos,vid).replace('.avi','_frames.csv'))
            print("--- Removing files done ---")
        
print("Done")
