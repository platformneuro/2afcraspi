# 2afcraspi
-----------

Code to run the 2AFC cage and to analyse the data.

## Running an experiment
### Overview
Start the raspberry pi  
Start the cage
Check that the sound amplifiers are on (3 amplifiers)  
Adjust the water level in the syringes to 50mL   
Manually turn on the pumps to check the water flow  
Weight the ferret
ssh to the raspberry pi
Check that the NAS data folder is mounted by:  
`cd /Desktop`
`ls` : the dataPi folder should have a green background  
If folder is not mounted:  
`cd /2afcraspi`
`./mount_datapi.sh`
Else:  
`cd 2afcraspi`  
Start experiment:
`python3 [script name] -a [animal name]`
At the of the experiment:  
- get the ferret out
- interrupt script (ctrl + c)  
Clean cage  

### Current scripts
#### Running the behavioral task
- shaping1.py
Habituate the ferret to the setup. Train them to go to the central port, wait for the stimulus, then go get a reward on both left and rigth ports.
- probing.py
Main script to train the ferret on the 2AFC task. This script can be used to play unrewarded probe stimuli to the ferret.
#### Utilities
- cage_test.py
Utility script for testing the different elements from the cage.
- mount_datapi.sh
Mount the NAS on the raspberry pi
- fuseCSV.py
merge raw data files belonging to the same session
- transfer_data.sh
Adds new sessions to the trial database.
- transfer_video.py
Compress videos of the session to h264 mp4 and copy them to the NAS

### parameter files
Several parameters of the behavioral session can be adjust by changing their values in the parameter file. A .yaml parameter file must be created at the root of /2afcraspi for each ferret with the folowing name structure : parameter_[ferret name].yaml.  

#### parameter template
`
# Stimuli
targets: # list of target stimuli, refering to the name of the folders containing them
 left: endowed
 right: endured
 probe: solicit

# For shaping - these parameters only apply to shaping1.py
shapingSound: testSoundStereo.wav
lateralPortReward: False # Water is released from both lateral ports after trial initiation
centralPortReward: False # Water is released at central port at trial initiation
# End of shaping parameters

# For training and probing - these parameters only apply to probing.py

# Method to pick target stimuli.
# Syntax : give index of the sound for left speaker, rigth speaker and probe
soundPickMethod:
 left: [0, 1, 3, 4, 2, 9, 10, 21, 11, 29, 12, 28, 14, 32, 17, 33, 18, 37, 27, 38] # Sounds used as left targets
 right: [0, 1, 2, 6, 7, 8, 12, 18, 19, 14, 17, 24, 27, 28, 30, 45, 31, 47, 39, 64] # Sounds used as right targets
 probe: 'none' # Probe sounds

trialProbabilities: # Probability to have a left, right or probe trial
 [0.5, 0.5, 0]

# Options guiding ferret behavior
## Loudness Cue
loudnessDifference: 10 # difference in dBSPL between left and right speakers, the louder on the target direction

## Correction trials 
correctionTrials: True # Activates / deactivates correction trial (i.e. replay the same stim n times after incorrect response)
nCorrectionTrials: 1 # Number (n) of consecutive replay of the sound

## Punishment
punishment: True # Punish incorrect responses with a timeout
punishmentDur: 5 # seconds, punishment duration

## Aborted trials
timeInPoke: 0.4 # second, time the animal has to stay in central poke

# General parameters
intervalDur: 60 # time (seconds) allowed to collect reward. Setup resets afterward.
solOpenDur: 0.6 # Reward duration (Solenoid open duration)
solOpenDurC: 0.6 # Reward duration at central port (Solenoid open duration)
solOpenDurR: 0.6 # Reward duration at right port (Solenoid open duration)
solOpenDurL: 0.5 # Reward duration at left port (Solenoid open duration)
minIPI: 0.05 # Minimal inter-poke interval (avoid multiple pokes being registered)
minILI: 0.05 # Minimal inter-lick interval (avoid multiple licks being registered)
rewardPortDelay: 0.08 # Reward ports must be activated for rewardPortDelay before registering
centralPortDelay: 0.08 # Reward ports must be activated for rewardPortDelay before registering 
maxRews: 3000 # Task terminates after this number of rewards

# Video parameters
recordVideo: False # Record video of ferret behavior
videoFPS: 15 # FPS of videos` 

### Adding new stimuli
The setup currently loads and plays .wav files. A new set of stimuli can be added to the setup by creating a folder in `/2afcraspi/stimuli/` populated with wav files.  
Reference to that folder will be explicit in the parameter file.

## Data analysis
### Data processing
The data files are stored locally in /2afcraspi/DataRaw and a copy is saved in /Desktop/datapy/Data/Animal/.
After discarding and merging files is necessary, the data can be processed and pushed to the NAS by running:
`./transfer_data.sh`  
In the data file, the name of the stimuli are saved.  
Avoid using non-alphanumeric caracters in the file names (i.e. _, -, /, ...) *as this can cause the analysis script to fail*.

### Data visualization
The online data visualization tool is accessible on line. Ask superviser for adress and login. Once the data is pushed on the NAS, it should be visible on line.

## Misc

### Calibrating the water reward
There is currently no automated way to calibrate the amount of water given on each trial. You can achieve calibration by opening the valves one minute and measuring the amount of water delivered. You can then adjust the solOpenDurL, solOpenDurC, solOpenDurR parameters to adjust the solenoid open duration to deliver the required amount of water.
We usually deliver 0.1 mL water per reward. 

### Code crash
Just restart the code. Is done fast enough, no need to put the ferret out of cage.  

### Video recording crash
It does not affect the session. Let the code run to the end of the behavioral session.

### Fuse sessions
In case of a code crash, the recording of the current training session may be split in two different files.  
Run `python fuseCSV.py` to select the files and merge them in one single data file.

### Discard a session
Any session can be discarded by adding _ignore.csv at the end of the file name.  
Exemple : Gouda_session268_230222.csv -> Gouda_session268_230222_ignore.csv  

### SSH connection lost
In case of connection lost, the script keeps running. The data will be saved properly, but you need to kill the python script manually at the end of the session (`kill -9 <PID>`), or reboot the raspberry (`sudo reboot -h now`).
Beware that video recording may continue and saturate the rpi memory.

### Setting up a new raspi
Install raspi OS (spec)
enable ssh connection
 sudo raspi-config
 select [3] Interface Options
 select SSH - enable
clone 2afcraspi repository on Desktop
 git clone https://Qneuron@bitbucket.org/platformneuro/2afcraspi.git
create dataPi folder on Desktop --> TO DO : automate that
install cifs utils
 sudo apt update
 sudo apt install samba samba-common-bin smbclient cifs-utils
mount remote data folder
 cd Desktop/2afcraspi
 ./mount_dataPi.sh
install python dependency
 pip install requirement.txt
upgrade numpy (for openCV)
 pip install -U numpy
Install psychopy
 see https://discourse.psychopy.org/t/unable-to-properly-install-psychopy-on-ubuntu-20-04/12760/8
 sudo apt-get install psychopy   # fetches outdated version from debian
 sudo apt-get install python3-pip python3-wxgtk-webview4.0 
 #update to latest psychopy:
 pip3 install -U psychopy  
 #fix some incompatible lib versions:
 pip3 install cffi==1.14.0 psychtoolbox==3.0.16 
copy sound stimuli in the stimuli folder
Have fun...

pandas == 1.1.3