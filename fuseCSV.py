import sys, os
import csv
import re
import tkinter as tk
from tkinter import filedialog
import numpy as np

#root = tk.Tk()
#root.withdraw()

def reorder(allPaths):
    sessionNB = list()
    for path in allPaths:
        sessionNB.append(int(re.findall('session([0-9]{1,3})',path)[0]))
    orderedPaths = list()
    for i in np.argsort(sessionNB):
        orderedPaths.append(allPaths[i])
    return orderedPaths

def fuse(allPaths):
    first = True
    timeCst = 0
    newFileName = allPaths[0].replace('.csv','_fused.csv')
    csvfile = open(newFileName,'w',newline='')
    filewriter = csv.writer(csvfile, delimiter =',', quotechar ='|', quoting=csv.QUOTE_NONE)

    for path in allPaths:
        csvIn = open(path)
        f = csv.reader(csvIn)
        firstData = True
        for row in f:
            newRow = list()
            # remove parameter line if exists
            if len(re.findall("rewList",row[0])) == 0:
                if not first:
                    continue
                else:
                    first = False

            if firstData: # Skip first line of actual data. May be corrupted.
                firstData = False
                continue

            for col in row:
                newCol = str()
                #splitCol = re.split(r"(\d+\.\d{3})", col)
                splitCol = re.split(r"(\d+\.)", col)
                for c in splitCol:
                    if c.endswith("."):
                        lastNum = int(c[:-1])
                        newCol = newCol + str(lastNum + timeCst) + "."
                    else:
                        newCol = newCol + c
                    
                newRow.append(newCol)
            # write modified row in csv
            filewriter.writerow(newRow)
        timeCst = round(lastNum) + 60 + timeCst
        # rename fused files
        csvIn.close()
        os.rename(path,path.replace('.csv','_ignore.csv'))
        print(os.path.basename(path) + "  ->  " + path.replace('.csv','_ignore.csv'))
    csvfile.close()
    print("fused in : " + newFileName)

if __name__ == "__main__":
    if len(sys.argv) > 1:
        allPaths = sys.argv[1:]
    else:
        #manual selection of files
        root = tk.Tk()
        root.withdraw()
        allPaths = filedialog.askopenfilenames(title="Select csv data files to fuse :", filetypes = [("csv files", ".csv")])
    if len(allPaths) > 0:
        allPaths = reorder(allPaths)
        print('Fusing:')
        for i in allPaths:
            print(i)
        fuse(allPaths)
    else:
        print('Operation cancelled.')
