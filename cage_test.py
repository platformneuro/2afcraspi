# import RPi.GPIO as GPIO
# import os
import time
import subprocess
import argparse
import sys
import AFCutils as fct

# Parse inputs
parser = argparse.ArgumentParser(description='Analyse sessions and add trials to the trial database')
parser.add_argument('-LED', dest='testLED', action='store_true', help='test LED.')
parser.add_argument('-valve', dest='testValve', action='store_true', help='test reward valves.')
parser.add_argument('-detector', dest='testDetector', action='store_true', help='test IR beams.')
parser.add_argument('-piezzo', dest='testPiezzo', action='store_true', help='test piezzo detectors on tubes.')
parser.add_argument('-sound', dest='testSound', action='store_true', help='test speakers (plays white noise).')
parser.add_argument('-stimuli', dest='testStimuli', default='0', type=str, help='test stimuli (loading and playing the specified target).')
parser.add_argument('-ssh', dest='ssh', default=1, help='are you connected through ssh ?')
args = parser.parse_args()

GPIO, led_pin, valve_pin, piezo_pin, detector_pin = fct.GPIOsetup()
portNames = ['left', 'center', 'right']

#time.sleep(30)

# Test detectors
# Initial state (should be true if aligned)
if args.testDetector:
    print('Testing the detectors')
    for c,i in enumerate(detector_pin):
            if GPIO.input(i) == False:
                #print('The IR beam sensor ' + str(i) + '(' + portNames[c] + ') is not  aligned')
                print('The IR beam sensor {} ({}) is not  aligned'.format(i, portNames[c]))
                sys.exit(1)
    # Response
    for c,i in enumerate(detector_pin):
        a = 0
        # print('Test detector ' + str(i))
        print('Test detector {}'.format(portNames[c]))
        while a == 0:
            if GPIO.input(i) == False:
                # print('detector ' + str(i) + ' ok')
                print('IR beam sensor {} OK'.format(portNames[c]))
                a = 1
    print('Done.')

# Test piezo
# Initial stage (should be false)
if args.testPiezzo:
    print('Testing the piezzo')
    for c,i in enumerate(piezo_pin):
            if GPIO.input(i) == True :
                print('Please make sure that ' + portNames[c] + ' piezo is well placed')
    # Response
    for c,i in enumerate(piezo_pin):
        a = 0
        print('Testing ' + portNames[c] + ' piezzo:')
        while a == 0 :
            if GPIO.input(i) == True :
                print('piezo detected')
                a = 1
    print('Done.')

# Test valves
if args.testValve:
    print('testing valves - get ready !')
    time.sleep(5)
    for c,i in enumerate(valve_pin):
        print('flushing ' + portNames[c] + ' valve for 3 seconds... ')
        GPIO.output(i,True)
        time.sleep(3)
        GPIO.output(i,False)
    print('Done.')

# Test sound
if args.testSound:
    print('Playing white noise :')
#os.system('omxplayer -o local ' + soundPath + '/' + soundNames[0]) #playing the sound
    #subprocess.call('aplay -c 1 -t wav ' + 'testSoundMono' + '.wav',shell=True) #Oxford way of p  laying sound
    #os.system('aplay -c 2 ' + 'testSoundStereo.wav')
    subprocess.call('aplay -c 2 ' + 'testSoundStereo.wav',shell=True)
    print('Done')

# Test led
if args.testLED:
    print('Testing LEDs')
    for c,i in enumerate(led_pin):
        print('LED ' + portNames[c] + ' on for 3 seconds')
        GPIO.output(i,True)
        time.sleep(3)
        GPIO.output(i,False)
    print('Done.')

# Test stimuli
if args.testStimuli != '0':
    print(args.testStimuli)
    stimPath = './stimuli'
    stimNames, waveObj = fct.bufferingSounds([args.testStimuli],stimPath)
    try:
        for i, l0 in enumerate(stimNames):
            for j, l1 in enumerate(l0):
                for k, l2 in enumerate(l1):
                    print('Playing : ' + stimNames[i][j][k])
                    waveObj[i][j][k].play()
                    time.sleep(2)
    except:
        print('keyboard interrupt.')


#GPIO.cleanup()

print('All test done')
