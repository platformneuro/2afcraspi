import simpleaudio as sa
import glob
import os
import time

targets = ('solicit',) # list of target words


print('buffering sounds...')
waveObj = list()
for i,target in enumerate(targets): 
    soundPathT = os.path.join(r'D:\LibriSpeech\code\stimuli\Twords',target)
    soundPathNT = os.path.join(r'D:\LibriSpeech\code\stimuli\NTwords',target)

#listoflists = []
#for i in range(0,2):
#    sublist = []
#    for j in range(0,10)
#        sublist.append((i,j))
#    listoflists.append(sublist)
#print listoflists

    Tlist = glob.glob(soundPathT + "/*.wav")
    NTlist = glob.glob(soundPathNT + '/*.wav')
    
    waveObjT = list()
    waveObjNT = list()
    for i,x in enumerate(NTlist):
        a = sa.WaveObject.from_wave_file(x)
        waveObjNT.append(a)
    for i,x in enumerate(Tlist):
        a = sa.WaveObject.from_wave_file(x)
        waveObjT.append(a)
    TNTwaveObj = list()
    TNTwaveObj.append(waveObjT)
    TNTwaveObj.append(waveObjNT)
    waveObj.append(TNTwaveObj)
    
print('done.')
print(len(waveObj[0][1]))
waveObj[0][1][6].play()

time.sleep(3)