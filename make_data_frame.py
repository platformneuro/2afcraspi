import os, re
import argparse
from datetime import datetime
import numpy as np
import pandas as pd
import loadBehavioralData as ld

# Parse inputs
parser = argparse.ArgumentParser(description='Analyse sessions and add trials to the trial database')
# parser.add_argument('-a', dest='animalName', default='testFerret', type=str, help='Animal''s name.')
parser.add_argument('-a','--list',nargs='+', dest='animalNames', default=['testFerret'], help='list of Animals names.')
parser.add_argument('-p', dest='dataPath', default="/home/lsp/Desktop/dataPi/Data", type=str, help='Path to the root folder of .csv data files .')
parser.add_argument('-pApp', dest='dataPathApp', default="/home/lsp/Desktop/dataPi", type=str, help='Path to 2afcapp data folder.')
# parser.add_argument('-o', dest='overwrite', default=0, type=int, help='Overwrite current trial database (0/1)')
parser.add_argument('-o', dest='overwrite', action='store_true', help='Flag. Overwrite current trial database.')
args = parser.parse_args()

# Loop over animals
for animalName in args.animalNames:
    print('Processing {}.'.format(animalName))
    path = os.path.join(args.dataPath, animalName)
    dbLoadFlag = 0
    # Load trials datafile
    if args.overwrite:
        lastSessionNB = 0
        print('Creating new data frame')
    else:
        print('Loading data frame...')
        try:
            allTrialsDF = pd.read_pickle(os.path.join(args.dataPathApp, animalName + '_allTrialsDF'))
            lastSessionNB = allTrialsDF['session nb'].max()
            dbLoadFlag = 1
            print('success !')
        except:
            lastSessionNB = 0
            print('failed. Creating a new one.')

    # Get file names
    fileList = sorted(os.listdir(path))

    # remove ignored files (merged leftover)
    fileList = [f for f in fileList if len(re.findall("_ignore", f)) == 0]
    sessionNB = list()
    sessionDate = list()
    sessionSplit = list()
    fileList_filt = list()
    for files in fileList:
        sNB = int(re.findall('session([0-9]{1,3})', files)[0])
        if sNB > lastSessionNB:
            print(files)
            sessionNB.append(sNB)
            fileList_filt.append(files)
            dateStr = re.findall('_([0-9]{6})', files)[0]
            sessionDate.append(datetime.strptime(dateStr, '%d%m%y'))
            sessionSplit.append(dateStr + '_' + str(sessionNB[-1]))
    fileList_filt[:] = [fileList_filt[i] for i in np.argsort(sessionNB)]
    sessionDate[:] = [sessionDate[i] for i in np.argsort(sessionNB)]
    sessionSplit[:] = [sessionSplit[i] for i in np.argsort(sessionNB)]
    sessionNB.sort()
    print(str(len(sessionNB)) + ' sessions will be added to the database.')

    # Analyze csv
    print('Analyzing data...')
    allTrials = list()
    allGoodTrials = list()
    for files in fileList_filt:
        print(files)
        filePath = os.path.join(path, files)
        dataObj = ld.dataManage(filePath, True)
        goodTrials = dataObj.getGoodTrials()
        allGoodTrials.append(goodTrials)

    print('Saving...')
    if dbLoadFlag:
        lastSessionNBcorr = allTrialsDF['session nb corr'].max()
        sessionDF = ld.trialDataFrame(allGoodTrials, lastSessionNBcorr + 1)
        allTrialsDF = allTrialsDF.append(sessionDF)
    else:
        sessionDF = ld.trialDataFrame(allGoodTrials)
        allTrialsDF = sessionDF

    allTrialsDF.to_pickle(os.path.join(args.dataPathApp, animalName + '_allTrialsDF'), protocol=3)

print('Done.')
