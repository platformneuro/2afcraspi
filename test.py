from threading import Thread
import queue
from time import sleep, time


error_q = queue.Queue()
start = time()
def fct(q,start):
    sleep(3)
    try:
        a = 1/0
    except Exception as e:
        print('error is now - ' + str(time()-start))
        q.put(e)
        sleep(3)

t = Thread(target=fct,args=(error_q,start))
t.start()
# t.join()

# start = time()
while time() - start < 8:
    # check if an exception was raised in the worker thread
    if not error_q.empty():
        # get the exception object from the queue and re-raise it in the main thread
        e = error_q.get()
        print(str(time()-start))
        print(e)
        break

sleep(1)
print('all done')