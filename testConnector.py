import billiard
import cv2
import time
#from multiprocessing import Process, Pipe

def f(conn):
    vid = cv2.VideoCapture(0)
    frame_width = int(vid.get(3))
    frame_height = int(vid.get(4))
    out = cv2.VideoWriter('./Data/outpy.avi',cv2.VideoWriter_fourcc('M','J','P','G'), 10, (frame_width,frame_height))
    
    while True:
        #print('in loop')
        a = conn.recv()
        print('a is ' + str(a))
        if not a:
    #        print('break loop')
            break
            
        ret, frame = vid.read()
        if ret == True: 
            out.write(frame)
        else:
            break    
         
    print('loop end')
    conn.close()
    # After the loop release the cap object 
    vid.release() 
    # Destroy all the windows 
    cv2.destroyAllWindows() 

if __name__ == '__main__':
    #a = True
    
    parent_conn, child_conn = billiard.Pipe()
    
    p = billiard.Process(target=f, args=(child_conn,))
    p.start()
    print('start')
    for i in range(300):
        print(i)
        parent_conn.send(True)
    #print(parent_conn.recv())   # prints "[42, None, 'hello']"
    #p.join()
    
    #print('here')
    #time.sleep(2)
    print('stop sig')
    parent_conn.send(False)
    time.sleep(2)
    
    
    p.terminate()
