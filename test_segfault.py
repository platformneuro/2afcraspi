import ctypes

# This code generates a segmentation fault
# Activate segfault handler by:
# python3 -X faulthandler test_segfault.py
print("Testing segfault :")

ctypes.string_at(0)
