# Mice Training code
# mice has to nose poke in center, reward is delivered on the right side for a
# 5 kHz tone and on the left side for a 7 kHz tone.
# licks in the next 5 sec

# coding: utf-8

# In[ ]:

# Quentin 2017

# Start of initialization
#------------------------------------------------------------------------------

print("Im online :)")

import numpy as np
import numpy.random as rnd
import time
import billiard
import RPi.GPIO as GPIO
#import pygame
import pickle
import csv
import requests as req
import subprocess
import socket
#------------------------------------------------------------------------------
# User Parameters

maxRews = 300 # Session is aborted after maxRews trials
intervalDur = 10 # in seconds, duration before trial interruption
LR_target = np.random.randint(2) # Initial target

sndLeft = "14_kHz"
sndRight = "5_kHz"
#------------------------------------------------------------------------------
#Initialise function for sending data to server

#Figure out appropriate IP address based on the server
pi_IP = [(s.connect(('8.8.8.8', 80)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1]
pi_ID = str(int(pi_IP[-3:])-100)


def send_data(load):

    headers = {'User-Agent': 'Mozilla/5.0'}
    link = 'http://192.168.0.99:8000/getData/' + pi_ID + '/get_PiData/'

    session = req.Session()
    r1 = session.get(link,headers=headers)

    link1 = 'http://192.168.0.99:8000/getData/' + pi_ID + '/write_PiData/'


    payload = {'piData':load,'csrfmiddlewaretoken':r1.cookies['csrftoken']}
    #cookies = dict(session.cookies)
    session.post(link1,headers=headers,data=payload)
    return None

#------------------------------------------------------------------------------

GPIO.setmode(GPIO.BOARD)

lickL = 38
lickR = 36
GPIO.setup(lickL,GPIO.IN)
GPIO.setup(lickR,GPIO.IN)
#GPIO.add_event_detect(lickL,GPIO.RISING)
#GPIO.add_event_detect(lickR,GPIO.RISING)

nosePk = 33

GPIO.setup(nosePk,GPIO.IN)
#GPIO.add_event_detect(nosePk,GPIO.RISING)

solOpenDur = 0.12
rewL = 37
rewR = 35
GPIO.setup(rewL,GPIO.OUT)
GPIO.setup(rewR,GPIO.OUT)

time.sleep(1)

#------------------------------------------------------------------------------

def deliverRew(channel):
    GPIO.output(channel,1)
    time.sleep(solOpenDur)
    GPIO.output(channel,0)

# Billiard process creation
rewProcR = billiard.Process(target=deliverRew,args=(rewR,))
rewProcL = billiard.Process(target=deliverRew,args=(rewL,))
#The mapping is 0 is right response, 1 is left response

#you might need to change rewProcL.start() to rewProcL.run()
def rew_action(side,rewProcR,rewProcL):
    if side==0:
        rewProcR.start()
    if side==1:
        rewProcL.start()
    LR_target = rnd.randint(2)
    return LR_target

def soundPlay(side):
    if side == 0:
        subprocess.call('aplay -c 1 -t wav ./' + sndRight + '.wav',shell=True)
    elif side == 1:
        subprocess.call('aplay -c 1 -t wav ./' + sndLeft + '.wav',shell=True)
    return None
# End of initialization
#------------------------------------------------------------------------------

print 'Starting session \n'

# Initialise variables
Training = True
nRews = 0

#Initialise lists for storage of reward and lick sides and times
lickList = [];  rewList = []; sndList = []
minILI = 0.01

#deliver reward centrally at beginning

#Initialise relevant timers
lickT = time.time(); prevL = time.time(); sendT = time.time()
lickTC = time.time()

#Define start time
start = time.time()

lateral_rew_available = False
trlCorr = True
firstL = True

#############################################
inPk = [False,False,False]
pkSt = [time.time(),time.time(),time.time()]
pked = [False,False,False]

minILI = 0.5
prevL = time.time()
rewT = time.time()
#############################################

while Training:
    # Control Sector to send data to webserver -----------------------------------------------------------------
    # if 5 seconds have elapsed since the last data_sending
    if (time.time()-sendT>5):

        lickStr = 'LickList:' + '-'.join([str(np.round(entry[0],decimals=3))+entry[1] for entry in lickList])
        rewStr = 'rewList:' + '-'.join([str(np.round(entry[0],decimals=3))+entry[1] for entry in rewList])
	sndStr = 'sndList:' + '-'.join([str(np.round(entry[0],decimals=3))+entry[1] for entry in sndList])
        sendStr = ','.join([rewStr,lickStr,sndStr])

        sendProc = billiard.Process(target=send_data,args=(sendStr,))
        sendProc.start()
        print 'seeeeeending'
        sendT = time.time()
        lickList = []; rewList = []; sndList = []

#Lick Detection and if appropriate reward delivery ------------------------------------------------------------

    if GPIO.input(lickL):
        if not inPk[2]:
            pkSt[2] = time.time()
            inPk[2] = True

        else:
            if ((time.time() - pkSt[2])>0.08 and not pked[2]):
                print 'lickL'
                lickT = time.time()
        	lickList.append([lickT -start,'L'])

                pked[2] = True
                if lateral_rew_available and (time.time()-rewT)>minILI:
                    if LR_target==1:
                        rewT = time.time()
                        LR_target = rew_action(1,rewProcR,rewProcL)
                        rewProcR = billiard.Process(target=deliverRew,args=(rewR,))
                        rewProcL = billiard.Process(target=deliverRew,args=(rewL,))

                    ###
                        nRews += nRews
                        rewList.append([time.time() - start,'L'])
                    lateral_rew_available = False
                    
                    print firstL
                    if firstL:
                        trlCorr=True
                    firstL = False
    else:
        inPk[2]=False
        pked[2] = False


    if GPIO.input(lickR):
        if not inPk[1]:
            pkSt[1] = time.time()
            inPk[1] = True

        else:
            if ((time.time() - pkSt[1])>0.08 and not pked[1]):
                print 'lickR'
                lickT = time.time()
		lickList.append([lickT -start,'R'])

                pked[1] = True
                if lateral_rew_available and (time.time()-rewT)>minILI:
                    if LR_target ==0:
                        rewT = time.time()
                        LR_target = rew_action(0,rewProcR,rewProcL)
                        rewProcR = billiard.Process(target=deliverRew,args=(rewR,))
                        rewProcL = billiard.Process(target=deliverRew,args=(rewL,))

                        nRews += nRews
                        rewList.append([time.time() - start,'R'])
                    lateral_rew_available = False
                    print firstL
                    if firstL:
                        trlCorr=True
                    firstL = False
    else:
        inPk[1]=False
        pked[1] = False

###################################################

    #if a central lick is detected immediately run code in if loop
    if GPIO.input(nosePk):
        #print "here"
        if not inPk[0]:
            pkSt[0] = time.time()
            inPk[0] = True

        else:
            if ((time.time() - pkSt[0])>0.08 and not pked[0]):
                pked[0] = True
        	print 'C'
                lickT = time.time()
                lickList.append([lickT -start,'C'])
                prevL = time.time()
                if not lateral_rew_available:
                    rewList.append([time.time() - start,'C'])
                    print trlCorr
                    #LR_target = np.random.randint(2) #Rand 0 or 1
                    firstL = True

                    lateral_rew_available = True
                    sndList.append([time.time()-start,str(LR_target)])
                    # soundPlay(LR_target)
                    sndProc = billiard.Process(target=soundPlay,args=(LR_target,))
                    sndProc.start()
                    lickTC = time.time()

    else:
        inPk[0]=False
        pked[0] = False


# Overall checks ------------------------------------------------------------

    # Check uncollected rewards
    if (time.time()-lickTC > intervalDur) and lateral_rew_available:
        lateral_rew_available = False

    # Check total number of rewards
    if nRews>maxRews:
        Training = False
