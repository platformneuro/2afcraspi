import os, sys
import subprocess
import argparse

# Convert videos in h264 format.
# Looks for .avi files, check for duplicates, and convert in same folder.

# Parse inputs
parser = argparse.ArgumentParser(description='Convert videos to mp4 h264 format.')
parser.add_argument('-p', dest='dataPath', default="/home/lsp/Desktop/2afcraspi/video", type=str, help='Path to video data files root folder.')
parser.add_argument('-rm', dest='remove', action='store_true', help='Remove source video after conversion.')
parser.add_argument('-cv', dest='convert', action='store_true', help='Convert videos to h264 mp4 format.')
parser.add_argument('-ft', dest='fileType', default='.avi', type=str, help='Filetype of original video files.')
args = parser.parse_args()

# Function definition
def stripVidNames(pathList):
    strPath = [os.path.splitext(f)[0] for f in pathList if '_frames' not in f]
    return strPath

# Check path validity
if not os.path.isdir(args.dataPath):
    raise TypeError('Invalid directory')

# Convert video(s)
# get video list
video_list = os.listdir(args.dataPath)
video_list_avi = [x for x in video_list if args.fileType in x]
video_list_mp4 = [x for x in video_list if '.mp4' in x]
video_list_unique = [x for x in video_list_avi if x.replace(args.fileType,'.mp4') not in video_list_mp4]

print('Videos to process : ' + str(video_list_unique))

# Safety warning
if not args.convert and args.remove:
    r = input('Warning, you are about to remove videos (-rm) without converting them (-cv). Do you want to proceed ? (Y/N) : ')
    if r != 'Y':
        sys.exit('User interruption')

# convert videos that are only present locally
for i,vid in enumerate(video_list_unique):
    if args.convert:
        print('\n--- Converting video {}/{} ---\n'.format(i+1,len(video_list_unique)))
        cmd = 'ffmpeg -i "{}" -vcodec h264 "{}"'.format(os.path.join(args.dataPath,vid), os.path.join(args.dataPath,vid).replace(args.fileType,".mp4"))
        out = subprocess.run(cmd,shell=True)
        print('--- Convertion done ---')

    # remove original videos from local folder
    if args.remove:
        print("--- Removing files ---")
        os.remove(os.path.join(args.dataPath,vid))
        print("--- Removing files done ---")
        
print("Done")

