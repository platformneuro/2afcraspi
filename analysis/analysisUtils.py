import pandas as pd
from scipy.special import ndtri

def filtTrialsDF_trialType(allTrialsDF,filters=('goodTrials')):
    # Trial filters
    TrialIdx = [False] * len(allTrialsDF)
    if 'correctionTrials' in filters:
        TrialIdx = TrialIdx | (allTrialsDF['correction trial'] == True)
    if 'abortedTrials' in filters:
        TrialIdx = TrialIdx | (allTrialsDF['aborted'] == True)
    if 'goodTrials' in filters:
        TrialIdx = TrialIdx | ((allTrialsDF['aborted'] == False) & (allTrialsDF['correction trial'] == False))
    allTrialsDF_filt = allTrialsDF[TrialIdx]
    return allTrialsDF_filt

def filtTrialsDF_dateRange(allTrialsDF,startDate,endDate):
    # Date filters
    dateIdx = allTrialsDF['date'].agg(lambda x: (x >= startDate) & (x <= endDate))
    allTrialsDF_filt = allTrialsDF[dateIdx]
    return allTrialsDF_filt

def filtTrialsDF_sessionNb(allTrialsDF, SessionList):
    
    mask= pd.DataFrame(False,index=allTrialsDF.index,columns=[allTrialsDF.columns[0]])
    mask = [False] * len(allTrialsDF)
    for s in SessionList:
        mask = mask | ((allTrialsDF['session nb corr'] == s))

    allTrialsDF_filt = allTrialsDF[mask]
    return allTrialsDF_filt

def getSessionDF(allTrialsDF):

    grouped = allTrialsDF.groupby(['session nb corr'])
    # grouped_unfilt = allTrialsDF[dateIdx].groupby(['session nb'])

    # BadTrialCount = grouped_unfilt['correction trial', 'aborted'].sum().astype(int)
    BadTrialCount = grouped[['correction trial', 'aborted']].sum().astype(int)

    leftPokes = grouped.apply(lambda x: (((x['stim'] == 0) & (x['hit'] == True)) | ((x['stim'] == 1) & (x['hit'] == False))).sum())

    # missCount = grouped['hit'].agg(lambda x: (x == False).sum()).reset_index().drop(columns='session nb corr')
    # missCount = missCount.rename(columns={"hit": "miss"})
    # hitCount = grouped['hit'].agg(lambda x: (x == True).sum()).reset_index().drop(columns='session nb corr')

    missCount = grouped['hit'].agg(lambda x: (x == False).sum())
    hitCount = grouped['hit'].agg(lambda x: (x == True).sum())

    hitCountT = grouped.apply(lambda x: pd.Series((x['stim'] == 0) & (x['hit'] == True)).sum())
    hitCountNT = grouped.apply(lambda x: pd.Series((x['stim'] == 1) & (x['hit'] == True)).sum())
    missCountT = grouped.apply(lambda x: pd.Series((x['stim'] == 0) & (x['hit'] == False)).sum())
    missCountNT = grouped.apply(lambda x: pd.Series((x['stim'] == 1) & (x['hit'] == False)).sum())

    dates = grouped['date'].first()

    sessionDF = pd.concat([hitCount, missCount, hitCountT, hitCountNT, missCountT, missCountNT, BadTrialCount, leftPokes, dates], axis=1)
    # sessionDF = sessionDF.rename(columns={'hit': 'hit', 'miss': 'miss', 0: 'hitTarget', 1: 'hitNonTarget', 2: 'missTarget', 3: 'missNonTarget', 4: 'NLeftPokes', 5: 'date'})
    sessionDF.columns = ['hit', 'miss', 'hitTarget', 'hitNonTarget', 'missTarget', 'missNonTarget', 'correction trial', 'aborted', 'NLeftPokes', 'date']
    sessionDF['NTrials'] = sessionDF['hit'] + sessionDF['miss']
    # sessionDF['hitRate'] = round(sessionDF['hit'] / sessionDF['NTrials'] * 100, 2)
    sessionDF['hitRateTarget'] = round(sessionDF['hitTarget'] / (sessionDF['hitTarget'] + sessionDF['missTarget']) * 100, 2)
    sessionDF['hitRateNonTarget'] = round(sessionDF['hitNonTarget'] / (sessionDF['hitNonTarget'] + sessionDF['missNonTarget']) * 100, 2)
    sessionDF['hitRate'] = round((sessionDF['hitTarget'] + sessionDF['hitNonTarget']) / (sessionDF['hitNonTarget'] + sessionDF['missNonTarget'] + sessionDF['hitTarget'] + sessionDF['missTarget']) * 100, 2)
    sessionDF['session number'] = sessionDF.index.values
    # sessionDF.set_index(dates, inplace=True)

    # sessionDF['d prime'] = sessionDF.apply(lambda x: ndtri(x['hitRateTarget']/100) - ndtri(1-(sessionDF['hitRateTarget']/100)))
    sessionDF['d prime'] = ndtri(sessionDF['hitRateTarget']/100) - ndtri(1-(sessionDF['hitRateTarget']/100))
    sessionDF['bias left'] = round(sessionDF['NLeftPokes'] / sessionDF['NTrials'] * 100, 2)

    # print(sessionDF['date'].head(10))

    groupedStim = allTrialsDF.groupby(['session nb corr', 'stimulus'])
    a = groupedStim['hit'].value_counts()
    stimNames = a.index.get_level_values('stimulus').unique().tolist()
    tList = [i for i in stimNames if 'solicit_' in i]
    for targ in tList:
        a_filt = a.xs(targ, level='stimulus')
        try:
            b = a_filt.xs(True, level='hit')
        except: # Rare case where no hit on this target happend across the sessions considered
            b = pd.Series(data=[0]*len(a_filt), index=a_filt.index.get_level_values('session nb corr'))
        try:
            c = a_filt.xs(False, level='hit')
        except: # Rare case where no hit on this target happend across the sessions considered
            c = pd.Series(data=[0]*len(a_filt), index=a_filt.index.get_level_values('session nb corr'))
        d = pd.concat([b,c], axis=1).fillna(0)
        d.columns = ['hit', 'miss']
        d['t_' + targ] = d['hit'] / (d['hit'] + d['miss']) * 100
        sessionDF = pd.concat([sessionDF, d['t_' + targ]], axis=1)

    return sessionDF