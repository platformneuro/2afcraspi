import cv2
import os
import numpy as np
import re
import csv

animalName = "testFerret"
fileName =  animalName + '_session213_310521'
videoPath = os.path.join('C:\\','Users','Quentin Gaucher','data','ENS behavior','video',animalName,fileName + '.avi')
dataPath = os.path.join('C:\\','Users','Quentin Gaucher','data','ENS behavior',animalName, fileName + '.csv')
framePath = videoPath.replace('.avi','_frames.csv')

timeOffset = 0

cap = cv2.VideoCapture(videoPath)
fps = cap.get(cv2.CAP_PROP_FPS)

frame_width = int(cap.get(3))
frame_height = int(cap.get(4))
out = cv2.VideoWriter(fileName + '_labelled.avi',cv2.VideoWriter_fourcc('M','J','P','G'), fps, (frame_width,frame_height))

# Reading data
f = csv.reader(open(dataPath))
frames = csv.reader(open(framePath))

rews = []
stims = []
licks = []
pokes = []

for row in f:
    for col in row:
        if len(re.findall("rewList:", col))>0:
            t = re.findall("rewList:(.*)", col)[0]
            if t!='':
                tmprews = t.split("-")
                tmprews = [x for x in tmprews if not ("C" in x)] # remove the "C" rewards from list
                rews = rews + tmprews
                
        if len(re.findall("sndList:", col))>0:
            t = re.findall("sndList:(.*)", col)[0]
            
            if t!='':
                stims = stims + t.split("-")
                
        if len(re.findall("lickList:", col))>0:
            t = re.findall("lickList:(.*)", col)[0]
            if t!='':
                t_sides = ["L" if "L" in i else "R" if "R" in i else "C" for i in t.split("-")]
                t_times = [float(re.findall("([0-9]{1,5}.[0-9]{1,5})[R,L,C]",i)[0]) for i in t.split("-")]
                t_ = [[i,j] for i,j in zip(t_times,t_sides)]
                licks = licks + t_
                
        if len(re.findall("pokeList:", col))>0:
            t = re.findall("pokeList:(.*)", col)[0]
            if t!='':
                t_sides = ["L" if "L" in i else "R" if "R" in i else "C" for i in t.split("-")]
                t_times = [float(re.findall("([0-9]{1,5}.[0-9]{1,5})[R,L,C]",i)[0]) for i in t.split("-")]
                t_ = [[i,j] for i,j in zip(t_times,t_sides)]
                pokes = pokes + t_

                
rew_sides = [re.findall("[0-9]{1,5}.{0,2}[0-9]{0,5}([A-z]*)",i)[0] for i in rews]
rew_t = [float(re.findall("([0-9]{1,5}.{0,2}[0-9]{0,5})[A-z]*",i)[0]) for i in rews]
stim_type = [int(re.findall("[0-9]{1,5}.{0,2}[0-9]{0,5}([0,1])",i)[0]) for i in stims]
stim_t = [float(re.findall("([0-9]{1,5}.{0,2}[0-9]{0,5})[A-z]*",i)[0]) for i in stims]
poke_t = [i[0] for i in pokes]
lick_t = [i[0] for i in licks]

# 0 - target - left

rew_sides = np.array(rew_sides)
rew_t = np.array(rew_t)
stim_type = np.array(stim_type)
stim_t = np.array(stim_t)
lick_t = np.array(lick_t)
poke_t = np.array(poke_t)

# Lick time and direction
licktC = np.array(lick_t)[np.where(np.array(licks)=='C')[0]]
licktR = np.array(lick_t)[np.where(np.array(licks)=='R')[0]]
licktL = np.array(lick_t)[np.where(np.array(licks)=='L')[0]]

# Pokes time and direction
pokesC = np.array(poke_t)[np.where(np.array(pokes)=='C')[0]]
pokesR = np.array(poke_t)[np.where(np.array(pokes)=='R')[0]]
pokesL = np.array(poke_t)[np.where(np.array(pokes)=='L')[0]]

txtPokesC = 'central poke'
txtPokesL = 'left poke'
txtPokesR = 'right poke'

allEventTimes = np.concatenate((pokesC, pokesL, pokesR))
allEventText = np.concatenate(([txtPokesC for i in range(len(pokesC))],[txtPokesL for i in range(len(pokesL))],[txtPokesR for i in range(len(pokesR))]))
allEventText = allEventText[np.argsort(allEventTimes)]
allEventTimes.sort()
allEventTimes = allEventTimes + timeOffset
#allEventsFrames = np.round(allEventTimes * fps)

# Load frames
allFramesTimes = list()
for row in frames:
    for col in row:
        allFramesTimes.append(float(col))
allFramesTimes = np.array(allFramesTimes)

allEventsFrames = list()
for i,t in enumerate(allEventTimes):
    dVect = abs(t - allFramesTimes)
    minVal = min(dVect)
    minIdx = np.argmin(dVect)
    allEventsFrames.append(minIdx)


f = 0
fLast = 0
eventIdx = -1
txtDuration = 10 # frames
NlastFrames = 30 # register 30 frames after last event, and cut video

# describe the type of font 
# to be used. 
font = cv2.FONT_HERSHEY_SIMPLEX

while(NlastFrames > 0): 
      
    # Capture frames in the video 
    ret, frame = cap.read()

    if not ret:
        break
    
    if np.in1d(f, allEventsFrames):
        # Use putText() method for 
        # inserting text on video
        eventIdx = eventIdx + 1
        fLast = txtDuration
        cv2.putText(frame,  
                    allEventText[eventIdx],  
                    (50, 50),  
                    font, 1,  
                    (0, 255, 255),  
                    2,  
                    cv2.LINE_4)
    
    if fLast > 0:
        fLast = fLast -1
        # Use putText() method for 
        # inserting text on video 
        cv2.putText(frame,  
                    allEventText[eventIdx],  
                    (50, 50),  
                    font, 1,  
                    (0, 255, 255),  
                    2,  
                    cv2.LINE_4)
  
    # write the resulting frame 
    #cv2.imshow('video', frame)
    out.write(frame)
    f = f + 1

    #if eventIdx > len(allEventsFrames):
    #    NlastFrames = NlastFrames - 1


# release the cap object 
cap.release() 
# close all windows 
cv2.destroyAllWindows() 

print(str(f) + ' frames saved.')