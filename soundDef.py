import os
import glob
import simpleaudio as sa
import numpy as np

stimPath = "E:\\LibriSpeech\\code\\stimuli"

indiv = "Gouda"

targets = ('solicit',) # list of target words
NVersion = 1 # Number of variant of each targets
soundPickMethod = ('fixed',4) # 'fixed', 'Ntargets', 'all'

print('buffering sounds...')
waveObj = list()
stimNames = list()
for i,target in enumerate(targets): 
    soundPathT = os.path.join(stimPath,'Twords',target)
    soundPathNT = os.path.join(stimPath,'NTwords',target)

    Tpaths = glob.glob(soundPathT + "/*.wav")
    NTpaths = glob.glob(soundPathNT + '/*.wav')
    Tlist = list()
    NTlist = list()
    for j,x in enumerate(Tpaths):
        Tlist.append(os.path.split(x)[-1][0:-4])
    for j,x in enumerate(NTpaths):
        NTlist.append(os.path.split(x)[-1][0:-4])
        
    thisNames = list()
    thisNames.append(Tlist)
    thisNames.append(NTlist)
    stimNames.append(thisNames)

    #tmp = os.path.split(stimNames[0][0][0])[-1][0:-4]

    waveObjT = list()
    waveObjNT = list()
    for i,x in enumerate(NTpaths):
        a = sa.WaveObject.from_wave_file(x)
        waveObjNT.append(a)
    for i,x in enumerate(Tpaths):
        a = sa.WaveObject.from_wave_file(x)
        waveObjT.append(a)
    TNTwaveObj = list()
    TNTwaveObj.append(waveObjT)
    TNTwaveObj.append(waveObjNT)
    waveObj.append(TNTwaveObj)
    
print('done.')


def pickSound(stimNames,soundPickMethod):
    # LR_target, targetID, soundID = pickSound(stimNames,soundPickMethod)
    LR_target = np.random.randint(2)
    targetID = np.random.randint(len(stimNames))
    if LR_target == 0: # target word
        if soundPickMethod[0] == 'fixed': # always pick the same sound
                soundID = soundPickMethod[1]
        elif soundPickMethod[0] == 'all': # pick any of available sounds
                soundID = np.random.randint(len(stimNames[targetID][LR_target]))
        elif soundPickMethod[0] == 'Ntargets': # pick one sound of the list
                N = len(soundPickMethod[1])
                soundID = soundPickMethod[1][np.random.randint(N)]
        else:
            print('Unexpected soundPickMethod')
                
    else: # Non target word
        soundID = np.random.randint(len(stimNames[targetID][LR_target])) # pick any non taget word
    return LR_target, targetID, soundID