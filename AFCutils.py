from logging import exception, raiseExceptions
import numpy as np
import os, sys
import glob
import time
import simpleaudio as sa
import datetime
import re
import csv
import natsort
import shutil
import argparse
import uuid
import wave
import yaml
#from probing import LR_target
#from pynput import keyboard
#import queue

# test #

### Variables ###
data_path = '/home/lsp/Desktop/2afcraspi/DataRaw' #path to where to put the data files of the experiment
stimPath = '/home/lsp/Desktop/2afcraspi/stimuli'
mount_path = '/home/lsp/Desktop/dataPi'


### Various checks ###
# Test psychopy
try:
    from psychopy import prefs
    prefs.hardware['audioLib'] = 'pygame'
    from psychopy import sound
    import wave
    psychopyload = True
except:
    print('could not import psychopy')
    psychopyload = False

# Check available space on the raspi
piMemory = shutil.disk_usage('/')
if piMemory[2] < 100000000: # Less than 1Gb
    print('Warning: there is less than 1Gb available space on disk.')
if piMemory[2] < 500000000:
    raise Exception('Memory full. You need at least 500 mb free memory to save video of a session.')

# Check if remote folder is mounted
if not os.path.ismount(mount_path):
    print('Warning : remote data folder is not mounted. Run ./mount_datapi.sh before running the experiment')
    r = input('Interrupt experiment (Y/N) ? ')
    if r == 'Y' or r == 'y':
        raise KeyboardInterrupt('User interruption')


### Functions ###

def read_param_file(animalName):
    with open(os.path.join('.','task_parameter_files','parameters_' + animalName + '.yaml')) as file:
        param = yaml.load(file, Loader=yaml.FullLoader)
    return param

def setParser():
    parser = argparse.ArgumentParser(description='Main script for training ferrets on 2AFC.')
    parser.add_argument('-a', dest='indiv', default='testFerret',type=str, help='Animal''s name.')
    parser.add_argument('-ssh', dest='SSH', default=1,type=int, help='Are you using SSH?')
    parser.add_argument('-noNAS', dest='noNAS', action='store_true', help='Flag to use local database in case of NAS failure.')
    return parser

def pickSound_probe(stimNames,soundPickMethod,sideProbabilities=[0.5,0.5,0]):
#     soundPickMethod:
#  left: [1,2,14]
#  right: 'all'
#  probe: 'none'
    targetID = np.random.randint(len(stimNames))
    nTarg = 3
    if soundPickMethod['probe'] == 'none':
        sideProbabilities = sideProbabilities[0:2]
        nTarg = 2
    # Pick target (left / right / probe) with given probabilities
    LR_target = np.random.choice(nTarg,1,replace = False, p = sideProbabilities)[0]
    targName = list(soundPickMethod.keys())[LR_target]
    targetList = soundPickMethod[targName]

    # Pick one sound if multiple sounds are available as targets.
    if targetList == 'all':
        soundID = np.random.randint(len(stimNames[targetID][LR_target]))
    else:
        N = len(targetList)
        soundID = targetList[np.random.randint(N)]
    return LR_target, targetID, soundID

def bufferingSounds_shaping(target, stimPath):
    stimNames = target.replace(".wav","")
    filePath = os.path.join(stimPath,"shapingSounds", target)
    with wave.open(filePath, "rb") as wave_file:
            fs = wave_file.getframerate()
    waveObj = sound.Sound(filePath,sampleRate=fs)
    return stimNames, waveObj

def bufferingSounds_new2(targets, stimPath, relativeLoudnessDifference = 0):
    # Sound setup
    # Generates a lists of all sounds handles and all sounds names with the following structure.
    # use as waveObj[target][T/NT/Probe][sound index]
    # |-target 1
    # |  |-Target sounds
    # |  |    |-Sound 1
    # |  |    |-Sound 2
    # |  |    |- ...
    # |  |-Non target sounds
    # |  |   |-Sound 1
    # |  |   |-Sound 2
    # |  |   |- ...
    # |  |-Probe stimuli (optional)
    # |  |   |-Sound 1
    # |  |   |-Sound 2
    # |  |   |-...
    # |-target 2
    # |  |-...

    print('buffering sounds...')
    waveObj = list()
    stimNames = list()
    soundPathT = os.path.join(stimPath,'Twords',targets['left'])
    soundPathNT = os.path.join(stimPath,'NTwords',targets['right'])
    soundPathProbe = os.path.join(stimPath,'Probes',targets['probe'])

    Tpaths = sorted(glob.glob(soundPathT + "/*.wav"))
    NTpaths = sorted(glob.glob(soundPathNT + '/*.wav'))
    Ppaths = sorted(glob.glob(soundPathProbe + '/*.wav'))

    Tlist = list()
    NTlist = list()
    Plist = list()
    for j,x in enumerate(Tpaths):
        Tlist.append(os.path.split(x)[-1][0:-4])
    for j,x in enumerate(NTpaths):
        NTlist.append(os.path.split(x)[-1][0:-4])
    for j,x in enumerate(Ppaths):
        Plist.append(os.path.split(x)[-1][0:-4])

    thisNames = list()
    thisNames.append(Tlist)
    thisNames.append(NTlist)
    thisNames.append(Plist)
    stimNames.append(thisNames)

    #tmp = os.path.split(stimNames[0][0][0])[-1][0:-4]

    waveObjT = list()
    waveObjNT = list()
    waveObjP = list()
    allFS = list()
    for i,x in enumerate(NTpaths):
        audio, fs = loadAndNormalizeSound(x, relativeLoudnessDifference = relativeLoudnessDifference, quietChannel = 0)
        allFS.append(fs)
        a = sound.Sound(audio,sampleRate=allFS[-1],stereo=True)
        #a = sa.play_buffer(audio,2,2,round(allFS[-1]))
        waveObjNT.append(a)
    for i,x in enumerate(Tpaths):
        audio, fs = loadAndNormalizeSound(x, relativeLoudnessDifference = relativeLoudnessDifference, quietChannel = 1)
        allFS.append(fs)
        a = sound.Sound(audio,sampleRate=allFS[-1], stereo = True)
        #a = sa.play_buffer(audio,2,2,round(allFS[-1]))
        waveObjT.append(a)
    for i,x in enumerate(Ppaths):
        audio, fs = loadAndNormalizeSound(x,relativeLoudnessDifference = 0, quietChannel = 1)
        allFS.append(fs)
        a = sound.Sound(audio,sampleRate=allFS[-1], stereo = True)
        #a = sa.play_buffer(audio,2,2,round(allFS[-1]))
        waveObjP.append(a)

    TNTwaveObj = list()
    TNTwaveObj.append(waveObjT)
    TNTwaveObj.append(waveObjNT)
    TNTwaveObj.append(waveObjP)
    waveObj.append(TNTwaveObj)

    if len(np.unique(allFS)) > 1:
        print('WARNING : inconsistant sound frame rate.')

    print('done.')
    return stimNames, waveObj


def loadAndNormalizeSound(soundPath, relativeLoudnessDifference = 0, quietChannel = 0):

    if quietChannel == 0:
        refChannel = 1
    else:
        refChannel = 0

    # Read file to get buffer                                                                                               
    ifile = wave.open(soundPath)
    samples = ifile.getnframes()
    nChannels = ifile.getnchannels()
    audio_in = ifile.readframes(samples)
    fs = ifile.getframerate()

    # Convert buffer to float32 using NumPy                                                                            
    audio_as_np_int16 = np.frombuffer(audio_in, dtype=np.int16)
    audio_as_np_float32 = audio_as_np_int16.astype(np.float32)

    # Normalise float32 array so that values are between -1.0 and +1.0                                                      
    max_int16 = 2**15
    audio_in = audio_as_np_float32 / max_int16

    # Channel check
    if nChannels == 1:
        # duplicate audio
        audio = np.array([audio_in,audio_in])
    elif nChannels == 2:
        # de-interleave the data
        audio = np.array([audio_in[0::2],audio_in[1::2]])
    else:
        raise ValueError('Only stereo or mono .wav files are supported')

    # Apply loudness difference in db SPL
    # Reminder : dbSPL = 20 * log10(rms(audioSignal)/p0)
    p0 = 2*10**-5 # Reference pressure level in Pa for dbSPL
    audio_rms = np.sqrt(np.mean(audio**2,axis=1))
    audio_dbSPL = 20 * np.log10(audio_rms/p0)
    target_rms = p0 * 10 ** ((audio_dbSPL[refChannel]-relativeLoudnessDifference)/20)
    audio[quietChannel] = audio[quietChannel] * (target_rms / audio_rms[refChannel])
    #print(np.mean(audio,axis = 1))
    audio = audio.transpose()
    audio = np.ascontiguousarray(audio)
    return audio, fs

def CreatDataFile(data_path, indiv, noNASflag) :
    # Take the name of the individual doing the session and the path where to put the datafile and create a csv file with the date
    # and the session number and initiate the colonnes of the information's recorded during the experiment. Will return the file used to write on the csv file, the csv file itself and the number of the session
    # input : -data_path : (string) the path where to put the file
    #         -indiv : (string) the name of the individual
    # output : -filewriter : (csv.writer object) file to write on
    #          -csv_file : (_io.TextIOWrapper object) the csv file
    #          -n_session : (float) the number of the session for a particular individual
    
    #find the date
    dateStart= datetime.datetime.now()
    year=(str(dateStart.year).split("0"))[1]
    month=str(dateStart.month)
    day= str(dateStart.day)
    if len(month)<2 :
        month="0"+month
    if len(day)<2:
        day="0"+day
    
    #find the session number
    data_files = natsort.natsorted(os.listdir(os.path.join(data_path,indiv))) # the list of files in the order of the number of session (in base 10)
    if len(data_files) != 0 :
        last_session = int(re.findall(r'\d+', data_files[-1].replace('_ignore','').replace('_fused','').split('_')[-2])[0])
    else:
        last_session = 0
    
    if ~noNASflag:
        data_files_NAS = natsort.natsorted(os.listdir(os.path.join(mount_path,'Data',indiv))) # the list of files on the NAS in the order of the number of session (in base 10)
    else:
        data_files_NAS = []
        Warning('no NAS flag is enabled.')
    if len(data_files_NAS) != 0:
        last_session_NAS = int(re.findall(r'\d+', data_files_NAS[-1].replace('_ignore','').replace('_fused','').split('_')[-2])[0])
    else:
        last_session_NAS = 0
    
    if last_session != last_session_NAS:
        Warning('Session numbers are inconsistent between local database and the mounted NAS. Please check manually.\n' +
            'last session local : {} | last session NAS : {}\n'.format(last_session,last_session_NAS) + 
            'Highest number kept.')
    
    n_session = max(last_session_NAS,last_session) + 1
        
    #create and initiate the csvfile
    csvfile = open(data_path + '/' + indiv + '/' + indiv + '_session' + str(n_session) + '_' + day + month + year + '.csv','w')
    #filewriter = csv.writer(csvfile, delimiter =',', quotechar ='|', quoting=csv.QUOTE_MINIMAL)
    filewriter = csv.writer(csvfile, delimiter =',', quotechar ='|',escapechar='"', quoting=csv.QUOTE_NONE)
    #filewriter.writerow(['all'])
    #filewriter.writerow(['Session','Trial','Sound category','Sound file','Behavior','Response','RT1(min): Start- SP','RT2(min) : Sounds - RP']) # the informations to catch for each trial
    csvfile.flush()
    
    # Get fileName
    fileName = csvfile.name
    fileName = fileName.split('/')
    fileName = fileName[-1][:-4]

    return filewriter, csvfile, n_session, fileName

def send_data(dataStr, filewriter, csvfile):
    filewriter.writerow(dataStr)
    csvfile.flush()

def rew_action(side,rewProcR,rewProcL):
    if side==0:
        rewProcL.start()
    if side==1:
        rewProcR.start()
    LR_target = np.random.randint(2)
    return LR_target

def deliverRew(channel,GPIO,solOpenDur):
    GPIO.output(channel,True)
    time.sleep(solOpenDur)
    GPIO.output(channel,False)
    while GPIO.input(channel) == 1:
        GPIO.output(channel,False)
        print('Trying to shut the valve')
        time.sleep(0.5)

def setParser():
    parser = argparse.ArgumentParser(description='Main script for training ferrets on 2AFC.')
    parser.add_argument('-a', dest='indiv', default='testFerret',type=str, help='Animal''s name.')
    parser.add_argument('-ssh', dest='SSH', default=1,type=int, help='Are you using SSH?')
    return parser

def writeInfoStr(param,data_filewriter,data_file):
    infoStr = ['script:' + sys.argv[0],
        'machine:' + getMacAddress(),
        'parameters:' + str(param)]
    print(infoStr)
    send_data(infoStr, data_filewriter, data_file)

def getMacAddress():
    macAddress = ':'.join(re.findall('..', '%012x' % uuid.getnode()))
    return macAddress

class KeyboardListenerHandle():

     def __init__(self):
#         self.key_listener = keyboard.Listener(on_press=self.on_press, on_release=self.on_release)
         self.inTest = True
        
#     # Setting up keyboard listener
#     def on_press(self,key):
#         pass
#         #print('{0} pressed'.format(key))

#     def on_release(self,key):
#         #print('{0} released'.format(key))
#         if key == keyboard.Key.esc:
#             print('terminating experiment')
#             self.inTest = False
#             exit(0)
#             return self

#     def start(self):
#         self.key_listener.start()
#         return self

# class KeyboardListenerHandleSSH():
    
#     q = queue.Queue()
#     with keyboard.Listener(
#             on_press=lambda k: q.put_nowait((k, True)),
#             on_release=lambda k: q.put_nowait((k, False))) as listener:
#         os.environ['DISPLAY'] = os.environ['REMOTE_DISPLAY']
#         controller = keyboard.Controller()
#         while True:
#             controller.touch(*q.get())


#     def __init__(self):
#         #self.key_listener = keyboard.Listener(on_press=self.on_press, on_release=self.on_release)
#         self.inTest = True
#         self.q = queue.Queue()
        
#     # Setting up keyboard listener
#     def on_press(self,key):
#         pass
#         #print('{0} pressed'.format(key))

#     def on_release(self,key):
#         #print('{0} released'.format(key))
#         if key == keyboard.Key.esc:
#             print('terminating experiment')
#             self.inTest = False
#             exit(0)
#             return self
#
     def start(self):
         self.inTest = True
#         self.key_listener.start()
         return self

# Set GPIO up
try:
    import RPi.GPIO as GPIO
    def GPIOsetup():
        # Pin setup
        led_pin = [5,6,13] #L,M,R
        valve_pin = [17,22,27]#l,m,r
        piezo_pin = [2,3,4]#l,m,r
        detector_pin = [10,9,11]#L,M,R

        # GPIO setup
        GPIO.setmode(GPIO.BCM)

        GPIO.setup(led_pin[0],GPIO.OUT,initial = GPIO.LOW)
        GPIO.setup(led_pin[1],GPIO.OUT,initial = GPIO.LOW)
        GPIO.setup(led_pin[2],GPIO.OUT,initial = GPIO.LOW)

        GPIO.setup(detector_pin[0],GPIO.IN)
        GPIO.setup(detector_pin[1],GPIO.IN)
        GPIO.setup(detector_pin[2],GPIO.IN)

        GPIO.setup(piezo_pin[0],GPIO.IN)
        GPIO.setup(piezo_pin[1],GPIO.IN)
        GPIO.setup(piezo_pin[2],GPIO.IN)

        GPIO.setup(valve_pin[0],GPIO.OUT,initial = GPIO.LOW)
        GPIO.setup(valve_pin[1],GPIO.OUT,initial = GPIO.LOW)
        GPIO.setup(valve_pin[2],GPIO.OUT,initial = GPIO.LOW)

        return GPIO, led_pin, valve_pin, piezo_pin, detector_pin
except:
    print('Could not set GPIO up.')




######### Not in use - Obsolete #########


# def test_memory():
#     a = shutil.disk_usage('/')
#     if a[2] < 100000000: # Less than 1Gb
#         print('Warning mem')
#     if a[2] < 700000000:
#         raise Exception('Memory full. You need at least 700 mb free memory to save video of a session.')

# def pickSound(stimNames,soundPickMethod):
#     # LR_target, targetID, soundID = pickSound(stimNames,soundPickMethod)
#     # Picksound method working with the yaml configuration files

#     LR_target = np.random.randint(2)
#     targetID = np.random.randint(len(stimNames))
    
#     if LR_target == 0: # target word
#         targetName = stimNames[targetID][LR_target][0]
#         targetName = targetName.split('_')[0] # only get the target name, not variant number
#         targetList = soundPickMethod[targetName]

#         if targetList == 'all': # pick any available target
#             soundID = np.random.randint(len(stimNames[targetID][LR_target]))
#         else: # pick from the indices provided in params
#             N = len(targetList)
#             soundID = targetList[np.random.randint(N)]

#     else: # Non target word
#         soundID = np.random.randint(len(stimNames[targetID][LR_target])) # pick any non target word
#     return LR_target, targetID, soundID

# def pickSound_old(stimNames,soundPickMethod):
#     # Old pickSound method, kept for backward compatibility.
#     # LR_target, targetID, soundID = pickSound(stimNames,soundPickMethod)
#     LR_target = np.random.randint(2)
#     targetID = np.random.randint(len(stimNames))
#     if LR_target == 0: # target word
#         if soundPickMethod[0] == 'fixed': # always pick the same sound
#                 soundID = soundPickMethod[1][0]
#         elif soundPickMethod[0] == 'all': # pick any of available sounds
#                 soundID = np.random.randint(len(stimNames[targetID][LR_target]))
#         elif soundPickMethod[0] == 'Ntargets': # pick one sound of the list
#                 N = len(soundPickMethod[1])
#                 soundID = soundPickMethod[1][np.random.randint(N)]
#         else:
#             print('Unexpected soundPickMethod')
                
#     else: # Non target word
#         soundID = np.random.randint(len(stimNames[targetID][LR_target])) # pick any non taget word
#     return LR_target, targetID, soundID


# def bufferingSounds_new(targets, stimPath):
#     # Sound setup
#     # Generates a lists of all sounds handles and all sounds names with the following structure.
#     # use as waveObj[target][T/NT/Probe][sound index]
#     # |-target 1
#     # |  |-Target sounds
#     # |  |    |-Sound 1
#     # |  |    |-Sound 2
#     # |  |    |- ...
#     # |  |-Non target sounds
#     # |  |   |-Sound 1
#     # |  |   |-Sound 2
#     # |  |   |- ...
#     # |  |-Probe stimuli (optional)
#     # |  |   |-Sound 1
#     # |  |   |-Sound 2
#     # |  |   |-...
#     # |-target 2
#     # |  |-...

#     print('buffering sounds...')
#     waveObj = list()
#     stimNames = list()
#     soundPathT = os.path.join(stimPath,'Twords',targets['left'])
#     soundPathNT = os.path.join(stimPath,'NTwords',targets['right'])
#     soundPathProbe = os.path.join(stimPath,'Probes',targets['probe'])

#     Tpaths = sorted(glob.glob(soundPathT + "/*.wav"))
#     NTpaths = sorted(glob.glob(soundPathNT + '/*.wav'))
#     Ppaths = sorted(glob.glob(soundPathProbe + '/*.wav'))

#     Tlist = list()
#     NTlist = list()
#     Plist = list()
#     for j,x in enumerate(Tpaths):
#         Tlist.append(os.path.split(x)[-1][0:-4])
#     for j,x in enumerate(NTpaths):
#         NTlist.append(os.path.split(x)[-1][0:-4])
#     for j,x in enumerate(Ppaths):
#         Plist.append(os.path.split(x)[-1][0:-4])

#     thisNames = list()
#     thisNames.append(Tlist)
#     thisNames.append(NTlist)
#     thisNames.append(Plist)
#     stimNames.append(thisNames)

#     #tmp = os.path.split(stimNames[0][0][0])[-1][0:-4]

#     waveObjT = list()
#     waveObjNT = list()
#     waveObjP = list()
#     allFS = list()
#     for i,x in enumerate(NTpaths):
#         # a = sa.WaveObject.from_wave_file(x)
#         with wave.open(x, "rb") as wave_file:
#             allFS.append(wave_file.getframerate())
#         a = sound.Sound(x,sampleRate=allFS[-1])
#         # audio, fs = loadAndNormalizeSound(x, relativeLoudnessDifference, quietChannel = 0)
#         # allFS.append(fs)
#         a = sound.Sound(audio,sampleRate=allFS[-1])
#         waveObjNT.append(a)
#     for i,x in enumerate(Tpaths):
#         #a = sa.WaveObject.from_wave_file(x)
#         with wave.open(x, "rb") as wave_file:
#             allFS.append(wave_file.getframerate())
#         a = sound.Sound(x,sampleRate=allFS[-1])
#         waveObjT.append(a)
#     for i,x in enumerate(Ppaths):
#         #a = sa.WaveObject.from_wave_file(x)
#         with wave.open(x, "rb") as wave_file:
#             allFS.append(wave_file.getframerate())
#         a = sound.Sound(x,sampleRate=allFS[-1])
#         waveObjP.append(a)
#     TNTwaveObj = list()
#     TNTwaveObj.append(waveObjT)
#     TNTwaveObj.append(waveObjNT)
#     TNTwaveObj.append(waveObjP)
#     waveObj.append(TNTwaveObj)

#     if len(np.unique(allFS)) > 1:
#         print('WARNING : inconsistant sound frame rate.')

#     print('done.')
#     return stimNames, waveObj

    
# def bufferingSounds(targets, stimPath):
#    #
#     # DEPRECATED
#     #    # Sound setup
#     # Generates a lists of all sounds handles and all sounds names with the following structure.
#     # use as waveObj[target][T/NT/Probe][sound index]
#     # |-target 1
#     # |  |-Target sounds
#     # |  |    |-Sound 1
#     # |  |    |-Sound 2
#     # |  |    |- ...
#     # |  |-Non target sounds
#     # |  |   |-Sound 1
#     # |  |   |-Sound 2
#     # |  |   |- ...
#     # |  |-Probe stimuli (optional)
#     # |  |   |-Sound 1
#     # |  |   |-Sound 2
#     # |  |   |-...
#     # |-target 2
#     # |  |-...

#     print('buffering sounds...')
#     waveObj = list()
#     stimNames = list()
#     for i,target in enumerate(targets):
#         soundPathT = os.path.join(stimPath,'Twords',target)
#         soundPathNT = os.path.join(stimPath,'NTwords',target)
#         soundPathProbe = os.path.join(stimPath,'Probes',target)

#         Tpaths = sorted(glob.glob(soundPathT + "/*.wav"))
#         NTpaths = sorted(glob.glob(soundPathNT + '/*.wav'))
#         Ppaths = sorted(glob.glob(soundPathProbe + '/*.wav'))

#         Tlist = list()
#         NTlist = list()
#         Plist = list()
#         for j,x in enumerate(Tpaths):
#             Tlist.append(os.path.split(x)[-1][0:-4])
#         for j,x in enumerate(NTpaths):
#             NTlist.append(os.path.split(x)[-1][0:-4])
#         for j,x in enumerate(Ppaths):
#             Plist.append(os.path.split(x)[-1][0:-4])

#         thisNames = list()
#         thisNames.append(Tlist)
#         thisNames.append(NTlist)
#         thisNames.append(Plist)
#         stimNames.append(thisNames)

#         #tmp = os.path.split(stimNames[0][0][0])[-1][0:-4]

#         waveObjT = list()
#         waveObjNT = list()
#         waveObjP = list()
#         for i,x in enumerate(NTpaths):
#             a = sa.WaveObject.from_wave_file(x)
#             waveObjNT.append(a)
#         for i,x in enumerate(Tpaths):
#             a = sa.WaveObject.from_wave_file(x)
#             waveObjT.append(a)
#         for x in Ppaths:
#             a = sa.WaveObject.from_wave_file(x)
#             waveObjP.append(a)
#         TNTwaveObj = list()
#         TNTwaveObj.append(waveObjT)
#         TNTwaveObj.append(waveObjNT)
#         TNTwaveObj.append(waveObjP)
#         waveObj.append(TNTwaveObj)

#         print('done.')
#         return stimNames, waveObj

# if psychopyload:
#     def bufferingSounds_psychopy(targets, stimPath):
#         # Sound setup
#         # Generates a lists of all sounds handles and all sounds names with the following structure.
#         # use as waveObj[target][T/NT/Probe][sound index]
#         # |-target 1
#         # |  |-Target sounds
#         # |  |    |-Sound 1
#         # |  |    |-Sound 2
#         # |  |    |- ...
#         # |  |-Non target sounds
#         # |  |   |-Sound 1
#         # |  |   |-Sound 2
#         # |  |   |- ...
#         # |  |-Probe stimuli (optional)
#         # |  |   |-Sound 1
#         # |  |   |-Sound 2
#         # |  |   |-...
#         # |-target 2
#         # |  |-...

#         print('buffering sounds...')
#         waveObj = list()
#         stimNames = list()
#         for i,target in enumerate(targets):
#             soundPathT = os.path.join(stimPath,'Twords',target)
#             soundPathNT = os.path.join(stimPath,'NTwords',target)
#             soundPathProbe = os.path.join(stimPath,'Probes',target)

#             Tpaths = sorted(glob.glob(soundPathT + "/*.wav"))
#             NTpaths = sorted(glob.glob(soundPathNT + '/*.wav'))
#             Ppaths = sorted(glob.glob(soundPathProbe + '/*.wav'))

#             Tlist = list()
#             NTlist = list()
#             Plist = list()
#             for j,x in enumerate(Tpaths):
#                 Tlist.append(os.path.split(x)[-1][0:-4])
#             for j,x in enumerate(NTpaths):
#                 NTlist.append(os.path.split(x)[-1][0:-4])
#             for j,x in enumerate(Ppaths):
#                 Plist.append(os.path.split(x)[-1][0:-4])

#             thisNames = list()
#             thisNames.append(Tlist)
#             thisNames.append(NTlist)
#             thisNames.append(Plist)
#             stimNames.append(thisNames)

#             #tmp = os.path.split(stimNames[0][0][0])[-1][0:-4]

#             waveObjT = list()
#             waveObjNT = list()
#             waveObjP = list()
#             for i,x in enumerate(NTpaths):
#                 a = sound.Sound(x,sampleRate=16000)
#                 waveObjNT.append(a)
#             for i,x in enumerate(Tpaths):
#                 a = sound.Sound(x,sampleRate=16000)
#                 waveObjT.append(a)
#             for i,x in enumerate(Ppaths):
#                 a = sound.Sound(x,sampleRate=16000)
#                 waveObjP.append(a)
#             TNTwaveObj = list()
#             TNTwaveObj.append(waveObjT)
#             TNTwaveObj.append(waveObjNT)
#             TNTwaveObj.append(waveObjP)
#             waveObj.append(TNTwaveObj)

#             print('done.')
#             return stimNames, waveObj

# def bufferingSounds_old(targets,stimPath):
#     # Sound setup
#     # Generates a lists of all sounds handles and all sounds names with the following structure.
#     # use as waveObj[target][T/NT/Probe][sound index]
#     # |-target 1
#     # |  |-Target sounds
#     # |  |    |-Sound 1
#     # |  |    |-Sound 2
#     # |  |    |- ...
#     # |  |-Non target sounds
#     # |  |   |-Sound 1
#     # |  |   |-Sound 2
#     # |  |   |- ...
#     # |  |-Probe stimuli (optional)
#     # |  |   |-Sound 1
#     # |  |   |-Sound 2
#     # |  |   |-...
#     # |-target 2
#     # |  |-...

#     print('buffering sounds...')
#     waveObj = list()
#     stimNames = list()
#     for i,target in enumerate(targets):
#         soundPathT = os.path.join(stimPath,'Twords',target)
#         soundPathNT = os.path.join(stimPath,'NTwords',target)

#         Tpaths = sorted(glob.glob(soundPathT + "/*.wav"))
#         NTpaths = sorted(glob.glob(soundPathNT + '/*.wav'))
#         Tlist = list()
#         NTlist = list()
#         for j,x in enumerate(Tpaths):
#             Tlist.append(os.path.split(x)[-1][0:-4])
#         for j,x in enumerate(NTpaths):
#             NTlist.append(os.path.split(x)[-1][0:-4])
            
#         thisNames = list()
#         thisNames.append(Tlist)
#         thisNames.append(NTlist)
#         stimNames.append(thisNames)

#         #tmp = os.path.split(stimNames[0][0][0])[-1][0:-4]

#         waveObjT = list()
#         waveObjNT = list()
#         for i,x in enumerate(NTpaths):
#             a = sa.WaveObject.from_wave_file(x)
#             waveObjNT.append(a)
#         for i,x in enumerate(Tpaths):
#             a = sa.WaveObject.from_wave_file(x)
#             waveObjT.append(a)
#         TNTwaveObj = list()
#         TNTwaveObj.append(waveObjT)
#         TNTwaveObj.append(waveObjNT)
#         waveObj.append(TNTwaveObj)

#         print('done.')
#         return stimNames, waveObj
