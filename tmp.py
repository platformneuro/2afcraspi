import os
import glob
import simpleaudio as sa


def bufferingSounds(targets,stimPath):
    # Sound setup
    # Generates a lists of all sounds handles and all sounds names with the following structure.
    # use as waveObj[target][T/NT][sound index]
    # |-target 1
    # |  |-Target sounds
    # |  |    |-Sound 1
    # |  |    |-Sound 2
    # |  |    |- ...
    # |  |-Non target sounds
    # |      |-Sound 1
    # |      |-Sound 2
    # |      |- ...
    # |-target 2
    # |  |-...

    print('buffering sounds...')
    waveObj = list()
    stimNames = list()
    for i,target in enumerate(targets): 
        soundPathT = os.path.join(stimPath,'Twords',target)
        soundPathNT = os.path.join(stimPath,'NTwords',target)

        Tpaths = glob.glob(soundPathT + "/*.wav")
        NTpaths = glob.glob(soundPathNT + '/*.wav')
        Tlist = list()
        NTlist = list()
        for j,x in enumerate(Tpaths):
            Tlist.append(os.path.split(x)[-1][0:-4])
        for j,x in enumerate(NTpaths):
            NTlist.append(os.path.split(x)[-1][0:-4])
            
        thisNames = list()
        thisNames.append(Tlist)
        thisNames.append(NTlist)
        stimNames.append(thisNames)

        #tmp = os.path.split(stimNames[0][0][0])[-1][0:-4]

        waveObjT = list()
        waveObjNT = list()
        for i,x in enumerate(NTpaths):
            a = sa.WaveObject.from_wave_file(x)
            waveObjNT.append(a)
        for i,x in enumerate(Tpaths):
            a = sa.WaveObject.from_wave_file(x)
            waveObjT.append(a)
        TNTwaveObj = list()
        TNTwaveObj.append(waveObjT)
        TNTwaveObj.append(waveObjNT)
        waveObj.append(TNTwaveObj)

        print('done.')
        return stimNames, waveObj
        