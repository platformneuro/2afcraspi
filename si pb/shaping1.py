import RPi.GPIO as GPIO
import os, sys
import time
import subprocess
import billiard
# import pickle
# import csv
import numpy as np
# import datetime
# import natsort
from videoGet4 import record
# import socket
# import re
# import numpy.random as rnd
# from pynput import keyboard
import argparse
import yaml
import shutil


#test param
# Parse inputs
parser = argparse.ArgumentParser(description='Main script for training ferrets on 2AFC.')
parser.add_argument('-a', dest='indiv', default='testFerret',type=str, help='Animal''s name.')
parser.add_argument('-ssh', dest='SSH', default=1,type=int, help='Are you using SSH?')
args = parser.parse_args()

data_path = '/home/lsp/Desktop/2afcraspi/Data' #path to where to put the data files of the experiment
stimPath = '/home/lsp/Desktop/2afcraspi/stimuli'
indiv = args.indiv
timeInPoke = 1 # second (son délai 2-3sec)

print(args.SSH)
if args.SSH==0:
    import AFCutils as fct
else:
    import AFCutilsSSH as fct

# Read paraneter file for that ferret
with open('parameters_' + indiv + '.yaml') as file:
    param = yaml.load(file, Loader=yaml.FullLoader)

tag = ['L','C','R']

# Sound setup
soundPath = ''
soundNames = ['testL.wav', 'testR.wav', 'testBilat.wav']

# Set pins
GPIO, led_pin, valve_pin, piezo_pin, detector_pin = fct.GPIOsetup()

# Billiard process creation
rewProcR = billiard.Process(target=fct.deliverRew,args=(valve_pin[2],GPIO,param['solOpenDurR']))
rewProcL = billiard.Process(target=fct.deliverRew,args=(valve_pin[0],GPIO,param['solOpenDurL']))
rewProcC = billiard.Process(target=fct.deliverRew,args=(valve_pin[1],GPIO,param['solOpenDurC']))

def soundPlay(side):
    if side == 0:
        #subprocess.call('aplay -c 1 -t wav ./7_kHz_long.wav',shell=True)
        #subprocess.call('aplay -c 2 ' + 'testSoundStereo.wav',shell=True)
        subprocess.call('aplay -c 2 ' + './stimuli/Twords/seq/testSoundStereo.wav',shell=True)
    elif side == 1:
        subprocess.call('aplay -c 2 ' + './stimuli/Twords/seq/testSoundStereo.wav',shell=True)
    return None

# keyboard listener
kb = fct.KeyboardListenerHandle().start()
# print(kb.inTest)
inTest = True
nRews = 0

#Initialise lists for storage of reward and lick sides and times
pokeList = rewList = sndList = lickList = [];

#Initialise relevant timers
pokeT = prevL = sendT = pokeTC = lickT = time.time()

LR_target = np.random.randint(2)
lateral_rew_available = False
trlCorr = True
firstL = True

# Left, Center, Rigth
inPk = [False,False,False]
pkSt = [time.time(),time.time(),time.time()]
pked = [False,False,False]
collected = [True,True]
hasPokedCentral = False
goSignal = False

prevL = time.time()
rewT = time.time()

#create a new data file for this session
data_filewriter,data_file,n_session,fileName = fct.CreatDataFile(data_path, indiv)
print(fileName)

# Write informations about the session
infoStr = ['script:' + sys.argv[0],
     'cTrial:' + str(param['correctionTrials']),
     'nCorrectionTrials:' + str(param['nCorrectionTrials']),
     'delay:' + str(param['timeInPoke']),
     'punishment:' + str(param['punishment']),
     'punishmentDur:' + str(param['punishmentDur']),
     'target:' + '-'.join(param['targets']),
     'soundPick:' + str(param['soundPickMethod'])]

print(infoStr)

fct.send_data(infoStr, data_filewriter, data_file)

# Start recording video
print('starting video feed...')
filepath = os.path.join('.', 'video', indiv, fileName)
video_getter = record(0, filepath, param['videoFPS']).start()

#Define start time
start = time.time()
nCorrectionTrialsDone = 0
trialCount = 0

print('ready !')

#Deliver an initial central reward
#_ = rew_action(2,rewProcR,rewProcL,rewProcC)
# rewList.append([time.time() - start,'C'])
try:
    while inTest:

        # Left poke
        if not GPIO.input(detector_pin[0]):
            if not inPk[0]:
                #print('here 1')
                pkSt[0] = time.time()
                inPk[0] = True

            else:
                if ((time.time() - pkSt[0])>0.08 and not pked[0]):
                    print('Poke Left')
                    pokeT = time.time()
                    pokeList.append([pokeT -start,'L'])

                    pked[0] = True
                    if lateral_rew_available and (time.time()-rewT)>param['minIPI']:
                        rewT = time.time()
                #_ = rew_action(1,rewProcR,rewProcL)
                #rewProcR = billiard.Process(target=deliverRew,args=(rewR,))
                        #rewProcL = billiard.Process(target=deliverRew,args=(rewL,))

                        nRews += nRews
                        rewList.append([time.time() - start,'L'])
                        #lateral_rew_available = False
                        collected[0] = True
                        print(firstL)
                        if firstL:
                            trlCorr=True
                        firstL = False
        else:
            inPk[0]=False
            pked[0] = False

        # Rigth poke
        if not GPIO.input(detector_pin[2]):
            if not inPk[2]:
                pkSt[2] = time.time()
                inPk[2] = True

            else:
                if ((time.time() - pkSt[2])>0.08 and not pked[2]):
                    print('poke Right')
                    pokeT = time.time()
                    pokeList.append([pokeT -start,'R'])

                    pked[2] = True
                    if lateral_rew_available and (time.time()-rewT)>param['minIPI']:
                        rewT = time.time()
                        #_ = rew_action(0,rewProcR,rewProcL)
                        #rewProcR = billiard.Process(target=deliverRew,args=(rewR,))
                        #rewProcL = billiard.Process(target=deliverRew,args=(rewL,))

                        nRews += nRews
                        rewList.append([time.time() - start,'R'])
                        #lateral_rew_available = False
                        collected[1] = True
                        print(firstL)
                        if firstL:
                            trlCorr=True
                        firstL = False
        else:
            inPk[2]=False
            pked[2] = False

        #central poke
        if not GPIO.input(detector_pin[1]):
            #print("here")
            #print((time.time() - pkSt[1]) > timeInPoke)
            if not inPk[1]:
                pkSt[1] = time.time()
                inPk[1] = True

            else:
                if ((time.time() - pkSt[1])>0.08 and not pked[1]):
                    print('poke Center')
                    #print(lateral_rew_available)
                    #print(collected)
                    #print(hasPokedCentral)
                    pked[1] = True
                    pokeT = time.time()
                    pokeList.append([pokeT -start,'C'])
                    prevL = time.time()
                    if (not lateral_rew_available and collected[0] and collected[1] and not hasPokedCentral):
                        rewList.append([time.time() - start,'C'])
                        GPIO.output(led_pin[1],True)
                        #print('LED on')
                        print(trlCorr)
                        LR_target = np.random.randint(2)
                        firstL = True
                        hasPokedCentral = True
                        #lateral_rew_available = True
                        # sndList.append([time.time()-start,str(LR_target)]) #+ "shapingStim" + "Aborted"
                        sndList.append([time.time()-start,str(LR_target) + 'shapingStimAborted'])
                        # sndList.append([pokeTC-start,str(LR_target)+stimNames[targetID][LR_target][soundID] + Ctstr + 'Aborted'])
                        # soundPlay(LR_target)
                        sndProc = billiard.Process(target=soundPlay,args=(LR_target,))
                        sndProc.start()
                        pokeTC = time.time()
                        goSignal = False

                elif (pked[1] and ((time.time() - pkSt[1])>timeInPoke) and not goSignal and not lateral_rew_available and collected[0] and collected[1] and hasPokedCentral):
                # keep in central poke, end of waiting time
                    GPIO.output(led_pin[1],False)
                    print('go get that reward !')
                    sndList[-1][1] = sndList[-1][1].replace('Aborted','')
                    goSignal = True
                    lateral_rew_available = True
                    collected = [False,False]
                    hasPokedCentral = False
                    
                    # Rearm processes
                    rewProcR = billiard.Process(target=fct.deliverRew,args=(valve_pin[2],GPIO,param['solOpenDurR']))
                    rewProcL = billiard.Process(target=fct.deliverRew,args=(valve_pin[0],GPIO,param['solOpenDurL']))

                    # Reward left and right ports
                    rewProcR.start()
                    rewProcL.start()

                    # Optional reward to central port
                    if param['centralPortReward']:
                        rewProcC = billiard.Process(target=fct.deliverRew,args=(valve_pin[1],GPIO,param['solOpenDurC'])) # essai pour avoir eau au port central, à retirer lors étape suivante shaping
                        rewProcC.start()
                    
        else:
            GPIO.output(led_pin[1],False)
            inPk[1]=False
            pked[1] = False
            goSignal = True

        # Count licks
        for i in range(3):
            if GPIO.input(piezo_pin[i]) == True and (time.time()-lickT)>param['minILI']:
                lickT = time.time()
                lickList.append([lickT - start,tag[i]])
                print('lick ' + tag[i])

        # reset hasPokeCentral timeInPoke seconds after central poke
        if (hasPokedCentral and (time.time()-pkSt[1] > 3)):
            print("reset trial")
            hasPokedCentral = False
            lateral_rew_available = False
            collected = [True,True]

        # Reset lateral reward
        if collected[0] and collected[1]:
            lateral_rew_available = False

        # Send data every 5 second
        if (time.time()-sendT>5) and goSignal:

            pokeStr = 'pokeList:' + '-'.join([str(np.round(entry[0],decimals=3))+entry[1] for entry in pokeList])
            rewStr = 'rewList:' + '-'.join([str(np.round(entry[0],decimals=3))+entry[1] for entry in rewList])
            sndStr = 'sndList:' + '-'.join([str(np.round(entry[0],decimals=3))+entry[1] for entry in sndList])
            lickStr = 'lickList:' + '-'.join([str(np.round(entry[0],decimals=3))+entry[1] for entry in lickList])
            #sendStr = ','.join([rewStr,pokeStr,sndStr,lickStr])
            sendStr = [rewStr,pokeStr,sndStr,lickStr]
            #print(pokeStr)
            #print(sendStr)

            sendProc = billiard.Process(target=fct.send_data,args=(sendStr,data_filewriter,data_file))
            sendProc.start()
            print('seeeeeending')
            sendT = time.time()
            pokeList = []; rewList = []; sndList = []; lickList = []

        # Check uncollected rewards
        if ((time.time()-pokeTC > param['intervalDur']) and lateral_rew_available):
            lateral_rew_available = False
            collected = [True,True]
            hasPokedCentral = False

        # Check total number of rewards
        if nRews>param['maxRews']:
            Training = False
except:
    #raise
    pass
# Sending last batch of data
pokeStr = 'pokeList:' + '-'.join([str(np.round(entry[0],decimals=3))+entry[1] for entry in pokeList])
rewStr = 'rewList:' + '-'.join([str(np.round(entry[0],decimals=3))+entry[1] for entry in rewList])
sndStr = 'sndList:' + '-'.join([str(np.round(entry[0],decimals=3))+entry[1] for entry in sndList])
lickStr = 'lickList:' + '-'.join([str(np.round(entry[0],decimals=3))+entry[1] for entry in lickList])
sendStr = [rewStr,pokeStr,sndStr,lickStr]

sendProc = billiard.Process(target=fct.send_data,args=(sendStr,data_filewriter,data_file))
sendProc.start()
GPIO.cleanup()

# Copy session file in Data raw
print('Copying raw data...')
shutil.copy2(os.path.join(data_path,indiv,fileName + '.csv'), os.path.join(data_path.replace('Data','DataRaw'),indiv,fileName + '.csv'))

# Stop video recording
video_getter.stop()
#print(video_getter.IOtime)

time.sleep(2)

print('All done')
