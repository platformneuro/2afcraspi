import AFCutilsSSH as fct

GPIO, led_pin, valve_pin, piezo_pin, detector_pin = fct.GPIOsetup()

for vId in valve_pin:
    GPIO.output(vId,False)

print('All valves have been manually turned off. Good luck.')
