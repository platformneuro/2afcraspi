import cv2
import time
import billiard

vid = cv2.VideoCapture(0) 

frame_width = int(vid.get(3))
frame_height = int(vid.get(4))

start = time.time()


# Define the codec and create VideoWriter object.The output is stored in 'outpy.avi' file.
out = cv2.VideoWriter('./Data/outpy.avi',cv2.VideoWriter_fourcc('M','J','P','G'), 10, (frame_width,frame_height))

def f():
    while(True):
        start = time.time()
        
        # Capture the video frame 
        # by frame 
        ret, frame = vid.read()
        if ret == True: 
            out.write(frame)
        else:
            break
        # Display the resulting frame 
        #cv2.imshow('frame', frame) 
          
        # the 'q' button is set as the 
        # quitting button you may use any 
        # desired button of your choice 

        #if cv2.waitKey(1) & 0xFF == ord('q'): 
        #    break
        #print(time.time()-start)
  

p = billiard.Process(target=f, args=())
print('starting video')
p.start()
time.sleep(5)
p.terminate()
print('done')


# After the loop release the cap object
ret, frame = vid.read()
if ret == True: 
    out.write(frame)
vid.release() 
# Destroy all the windows 
cv2.destroyAllWindows() 


