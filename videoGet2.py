from threading import Thread
#import billiard
import cv2
#import os
from time import time

class video:
    """
    Class that continuously gets frames from a VideoCapture object
    with a dedicated thread.
    """

    def __init__(self, src=0,filepath='./outpy',fps=0):
        #self.stream = cv2.VideoCapture(src,cv2.CAP_DSHOW)
        self.stream = cv2.VideoCapture(src)
        self.frame_width = int(self.stream.get(3))
        self.frame_height = int(self.stream.get(4))
        (self.grabbed, self.frame) = self.stream.read()
        cameraFps = self.stream.get(cv2.CAP_PROP_FPS)
        #self.stream.set(cv2.CAP_PROP_BUFFERSIZE,2)
        self.IOtime = list()
        
        
        # Check video fps
        if cameraFps == 0:
            print("warning : could not read camera fps.")
            if fps == 0:
                raise ValueError('Set fps is too low.')
        else:
            if fps == 0:
                fps = cameraFps
            if fps > cameraFps:
                raise ValueError('set fps is superior to camera fps.')

        self.frameDuration = 1/fps
        self.stopped = False
        self.out = cv2.VideoWriter(filepath + '.avi',cv2.VideoWriter_fourcc('M','J','P','G'),fps, (self.frame_width,self.frame_height))
        #self.out = cv2.VideoWriter(filepath + '.avi',cv2.VideoWriter_fourcc('a','v','c','1'),fps, (self.frame_width,self.frame_height))
        self.startTime = time()
        self.currentTime = time()
        self.IOtime = list()
        self.NframeRed = 0
        self.NframeDisp = 0
        self.NframeRec = 0
        # start reading frames
        self.newFrameR = False
        self.newFrameD = False
        Thread(target=self.get, args=()).start()

    def record(self):
        self.stopRec = False
        Thread(target=self.rec, args=()).start()
        #billiard.Process(target=self.get, args=()).start()
        return self

    def display(self):
        self.stopDisp = False
        Thread(target=self.disp, args=()).start()
        return self

    def get(self):
        while not self.stopped:
            if not self.grabbed:
                print("Grab failed.")
                self.stop()
            else:
                if (time() - self.currentTime) > self.frameDuration:
                    self.currentTime = time()
                    #self.IOtime.append(time()-self.startTime)
                    (self.grabbed, self.frame) = self.stream.read()
                    self.newFrameR = True
                    self.newFrameD = True
                    self.NframeRed += 1
        else:
            self.endStream()

    def rec(self):
        while not self.stopRec:
            if self.newFrameR:
                self.out.write(self.frame)
                self.newFrameR = False
                self.NframeRec += 1
                
    def disp(self):
        while not self.stopDisp:
            if self.newFrameD:
                cv2.imshow('video stream',self.frame)
                cv2.waitKey(1)
                self.newFrameD = False
                self.NframeDisp += 1
            
    def stop(self):
        self.stopped = True
        self.stopDisp = True
        self.stopRec = True
        self.endTime = time()
    
    def endStream(self):
        print("Video recording ended.")
        (self.grabbed, self.frame) = self.stream.read()
        self.stream.release()
        self.out.release()
        cv2.destroyAllWindows()
 

