#import RPi.GPIO as GPIO
import os, sys
import time
# import subprocess
import billiard
# import pickle
# import csv
import numpy as np
# import datetime
#import natsort
# import socket
# import re
# import numpy.random as rnd
# from pynput import keyboard
import simpleaudio as sa
# import glob
# import cv2
from videoGet3 import record
import AFCutils as fct

# Training. The animal has to report left for targets and rigth for non target words
# Can adjust the number of repeat of the correction trial
# Can adjust the duration of punishment if incorrect response

data_path = '/home/lsp/Desktop/2afcraspi/Data' #path towhere to put the data files of the experiment
stimPath = '/home/lsp/Desktop/2afcraspi/stimuli'
indiv = "Bouton"

if indiv == "Gouda":
    targets = ('solicit',) # list of target words
    # soundPickMethod = ('fixed',4) # Method to pick target stimuli. ('fixed',stimID) | ('Ntargets',(ID1,ID2...)) | ('all',)
    soundPickMethod = ('Ntargets',(4,1)) # Method to pick target stimuli. ('fixed',stimID) | ('Ntargets',(ID1,ID2...)) | ('all',)
    correctionTrials = False # Activates / deactivates correction trial (ie replay stim once after wrong trial)
    nCorrectionTrials = 1
    punishment = True
    punishmentDur = 5 # seconds
    timeInPoke = 0.75 # second
    
elif indiv == "Bouton":
    targets = ('solicit',) # list of target words
    soundPickMethod = ('fixed',(4,)) # Method to pick target stimuli. ('fixed',stimID) | ('Ntargets',(ID1,ID2...)) | ('all',)
    correctionTrials = False # Activates / deactivates correction trial (ie replay stim once after wrong trial)
    nCorrectionTrials = 1
    punishment = False
    punishmentDur = 5 # seconds
    timeInPoke = 0.75 # second
    
elif indiv == "testFerret":
    targets = ('solicit',) # list of target words
    # soundPickMethod = ('fixed',4) # Method to pick target stimuli. ('fixed',stimID) | ('Ntargets',(ID1,ID2...)) | ('all',)
    soundPickMethod = ('Ntargets',(4,1)) # Method to pick target stimuli. ('fixed',stimID) | ('Ntargets',(ID1,ID2...)) | ('all',)
    correctionTrials = True # Activates / deactivates correction trial (ie replay stim once after wrong trial)
    nCorrectionTrials = 1
    punishment = True
    punishmentDur = 5 # seconds
    timeInPoke = 0.75 # second
       
else:
    print("Unknown ferret. Abort.")
    exit()

inTest = True
maxRews = 3000; nRews = 0
intervalDur = 60 # time (seconds) allowed to collect reward. Setup resets afterward.
tag = ['L','C','R']
solOpenDur = 0.6
minIPI = 0.05
minILI = 0.05
rewardPortDelay = 0.08 # Reward ports must be activated for rewardPortDelay before registering
centralPortDelay = 0.08 # Reward ports must be activated for rewardPortDelay before registering 

videoFPS = 30

# Load sounds
stimNames, waveObj = fct.bufferingSounds(targets,stimPath)

# Set pins
GPIO, led_pin, valve_pin, piezo_pin, detector_pin = fct.GPIOsetup()


# Billiard process creation
rewProcR = billiard.Process(target=fct.deliverRew,args=(valve_pin[2],GPIO,solOpenDur))
rewProcL = billiard.Process(target=fct.deliverRew,args=(valve_pin[0],GPIO,solOpenDur))
rewProcC = billiard.Process(target=fct.deliverRew,args=(valve_pin[1],GPIO,solOpenDur))

# keyboard listener
kb = fct.KeyboardListenerHandle().start()


#Initialise lists for storage of reward and lick sides and times
pokeList = []
rewList = []
sndList = []
lickList = []

#Initialise relevant timers
pokeT = time.time(); prevL = time.time(); sendT = time.time()
pokeTC = time.time()
lickT = time.time()

LR_target = np.random.randint(2)
lateral_rew_available = False

# Left, Center, Rigth
inPk = [False,False,False]
pkSt = [time.time(),time.time(),time.time()]
pked = [False,False,False]
collected = [True,True]
cTrial = False
hasPokedCentral = False
inPunishment = False
goSignal = True
prevL = time.time()
rewT = time.time()

#create a new data file for this session
data_filewriter,data_file,n_session,fileName = fct.CreatDataFile(data_path, indiv)
print(fileName)

# Write informations about the session
infoStr = ['script:' + sys.argv[0],
     'cTrial:' + str(correctionTrials),
     'nCorrectionTrials:' + str(nCorrectionTrials),
     'delay:' + str(timeInPoke),
     'punishment:' + str(punishment),
     'punishmentDur:' + str(punishmentDur),
     'target:' + '-'.join(targets),
     'soundPick:' + soundPickMethod[0],
     'targetID:' + '-'.join([str(i) for i in soundPickMethod[1]])]
fct.send_data(infoStr, data_filewriter,data_file)

# Start recording video
print('starting video feed...')
filepath = os.path.join('.','video',indiv,fileName)
video_getter = record(0,filepath,videoFPS).start()

#Define start time
start = time.time()
nCorrectionTrialsDone = 0
trialCount = 0

print('ready !')
while inTest:
    time.sleep(0.002)
    # Left poke
    if not GPIO.input(detector_pin[0]):
        if not inPk[0]:
            #print('here 1')
            pkSt[0] = time.time()
            inPk[0] = True

        else:
            if ((time.time() - pkSt[0])>rewardPortDelay and not pked[0]):
                print('Poke Left')
                pokeT = time.time()
                pokeList.append([pokeT -start,'L'])

                pked[0] = True
                if lateral_rew_available and (time.time()-rewT)>minIPI:
                    if LR_target == 0: # good response
                        print('good response')
                        cTrial = False
                        rewT = time.time()
                        _ = fct.rew_action(0,rewProcR,rewProcL)
                        rewProcR = billiard.Process(target=fct.deliverRew,args=(valve_pin[2],GPIO,solOpenDur))
                        rewProcL = billiard.Process(target=fct.deliverRew,args=(valve_pin[0],GPIO,solOpenDur))

                        nRews += 1
                        rewList.append([rewT - start,'L'])
                        lateral_rew_available = False
                        # collected[0] = True
                    elif LR_target == 1: # wrong response
                        print('wrong response')
                        if nCorrectionTrialsDone < nCorrectionTrials:
                            cTrial = True
                        else:
                            cTrial = False
                        if punishment:
                            inPunishment = True
                            punishmentStart = time.time()
                            print('Punished for ' + str(punishmentDur) + 'sec !')
                        lateral_rew_available = False

    else:
        inPk[0]=False
        pked[0] = False

    # Rigth poke
    if not GPIO.input(detector_pin[2]):
        if not inPk[2]:
            pkSt[2] = time.time()
            inPk[2] = True

        else:
            if ((time.time() - pkSt[2])>rewardPortDelay and not pked[2]):
                print('poke Right')
                pokeT = time.time()
                pokeList.append([pokeT -start,'R'])

                pked[2] = True
                if lateral_rew_available and (time.time()-rewT)>minIPI:
                    if LR_target==1: # good response
                        print('good response')
                        cTrial = False
                        rewT = time.time()
                        _ = fct.rew_action(1,rewProcR,rewProcL)
                        rewProcR = billiard.Process(target=fct.deliverRew,args=(valve_pin[2],GPIO,solOpenDur))
                        rewProcL = billiard.Process(target=fct.deliverRew,args=(valve_pin[0],GPIO,solOpenDur))

                        nRews += 1
                        rewList.append([time.time() - start,'R'])
                        lateral_rew_available = False
                        # collected[1] = True
                    elif LR_target==0: # wrong response
                        print('wrong response')
                        if nCorrectionTrialsDone < nCorrectionTrials:
                            cTrial = True
                        else:
                            cTrial = False
                        if punishment:
                            inPunishment = True
                            punishmentStart = time.time()
                            print('Punished for ' + str(punishmentDur) + 'sec !')
                        lateral_rew_available = False 

    else:
        inPk[2]=False
        pked[2] = False

    #central poke
    if not GPIO.input(detector_pin[1]):
        #print("here")
        if not inPk[1]:
            pkSt[1] = time.time()
            inPk[1] = True

        else:
            if ((time.time() - pkSt[1])>centralPortDelay and not pked[1] and not hasPokedCentral):
                print('poke Center')
                pked[1] = True
                hasPokedCentral = True
                pokeT = time.time()
                pokeList.append([pokeT - start,'C'])
                prevL = time.time()
                if not lateral_rew_available and not inPunishment:
                    if correctionTrials and cTrial:
                        nCorrectionTrialsDone = nCorrectionTrialsDone + 1
                        Ctstr = 'CoTr'
                    else:
                        LR_target, targetID, soundID = fct.pickSound(stimNames,soundPickMethod)
                        nCorrectionTrialsDone = 0
                        Ctstr = ''
                    rewList.append([time.time() - start,'C'])
                    print('playing ' + stimNames[targetID][LR_target][soundID] + ' - ' + Ctstr)
                    trialCount += 1
                    waveObj[targetID][LR_target][soundID].play()
                    pokeTC = time.time()
                    sndList.append([pokeTC-start,str(LR_target)+stimNames[targetID][LR_target][soundID] + Ctstr + 'Aborted'])
                    goSignal = False
            
            elif (pked[1] and (time.time() - pkSt[1])>timeInPoke) and not inPunishment and not goSignal: # keep in central poke, end of waiting time
            #elif (pked[1] and ((time.time() - pkSt[1])>timeInPoke) and not goSignal and not lateral_rew_available and collected[0] and collected[1] and hasPokedCentral):
            
                print('go get that reward !')
                sndList[-1][1] = sndList[-1][1].replace('Aborted','') 
                goSignal = True
                lateral_rew_available = True
                hasPokedCentral = False
                    
    else:
        inPk[1]=False
        pked[1] = False

    # reset hasPokeCentral timeInPoke seconds after central poke
    if (hasPokedCentral and (time.time()-pkSt[1] > (timeInPoke + 2))):
        hasPokedCentral = False
        #if cTrial == True:
        #    cTrial = False
            
    # Reset punishment
    if punishment and inPunishment:
        if (time.time() - punishmentStart) > punishmentDur:
            inPunishment = False
            print('Punishment is over.')

    # Count licks
    for i in range(3):
        if GPIO.input(piezo_pin[i]) == True and (time.time()-lickT)>minILI:
            lickT = time.time()
            lickList.append([lickT - start,tag[i]])
            print('lick ' + tag[i])

    # Send data every 5 second 
    if (time.time()-sendT>5) and goSignal: 
    
        pokeStr = 'pokeList:' + '-'.join([str(np.round(entry[0],decimals=3))+entry[1] for entry in pokeList])
        rewStr = 'rewList:' + '-'.join([str(np.round(entry[0],decimals=3))+entry[1] for entry in rewList])
        sndStr = 'sndList:' + '-'.join([str(np.round(entry[0],decimals=3))+entry[1] for entry in sndList])
        lickStr = 'lickList:' + '-'.join([str(np.round(entry[0],decimals=3))+entry[1] for entry in lickList])
        #sendStr = ','.join([rewStr,pokeStr,sndStr,lickStr])
        sendStr = [rewStr,pokeStr,sndStr,lickStr]
        #print(pokeStr)
        #print(sendStr)
        
        sendProc = billiard.Process(target=fct.send_data,args=(sendStr,data_filewriter,data_file))
        sendProc.start()
        print('seeeeeending')
        sendT = time.time()
        pokeList = []; rewList = []; sndList = []; lickList = []

    # Check uncollected rewards
    if (time.time()-pokeTC > intervalDur) and lateral_rew_available:
        lateral_rew_available = False
        collected = [True,True,True]

    # Check total number of rewards
    if nRews>maxRews:
        inTest = False
        
    # Check if idle for more than 10 minutes (forget to turn task off)
    if time.time() - pokeT > 600:
        inTest = False
    
    if not kb.inTest:
        inTest = False
        

# Sending last batch of data
pokeStr = 'pokeList:' + '-'.join([str(np.round(entry[0],decimals=3))+entry[1] for entry in pokeList])
rewStr = 'rewList:' + '-'.join([str(np.round(entry[0],decimals=3))+entry[1] for entry in rewList])
sndStr = 'sndList:' + '-'.join([str(np.round(entry[0],decimals=3))+entry[1] for entry in sndList])
lickStr = 'lickList:' + '-'.join([str(np.round(entry[0],decimals=3))+entry[1] for entry in lickList])
sendStr = [rewStr,pokeStr,sndStr,lickStr]

sendProc = billiard.Process(target=fct.send_data,args=(sendStr,data_filewriter,data_file))
sendProc.start()

# Cleaning GPIO pins
GPIO.cleanup()

# Stop video recording
video_getter.stop()
#print(video_getter.IOtime)
time.sleep(2)
print('Trial done : ' + str(trialCount))
print('All done')
